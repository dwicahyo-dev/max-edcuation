<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->login_checker->check_login_admin();
	}

	public function index()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Halaman Career',
			'content' => $this->load->view('admin/pages/career/content_career_view', [
				'careers' => $this->Career_model->get_careers(),
			], TRUE),
			
			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function career_add()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Halaman Career - Add Career',
			'content' => $this->load->view('admin/pages/career/content_add_career_view', [
				'career_job_positions' => $this->Career_model->get_career_job_positions(),
				'career_job_areas' => $this->Career_model->get_career_job_areas(),
			], TRUE),
			
			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function get_career_where($id_career){
		$career = $this->Career_model->get_career_where($id_career);

		if ($career) {
			$this->output->set_content_type('application/json')->set_output(json_encode($career));
		}
	}

	public function career_edit($id_career)
	{
		$data = [
			'title' => 'MAX Education | Admin - Edit Data Halaman Career',
			'content' => $this->load->view('admin/pages/career/content_edit_career_view', [
				'career_job_positions' => $this->Career_model->get_career_job_positions(),
				'career_job_areas' => $this->Career_model->get_career_job_areas(),
				'career' => $this->Career_model->get_career_where($id_career),
			], TRUE),

			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),

		];

		$this->load->view('admin/index', $data);
	}

	public function career_delete($id_career)
	{
		$image_file_name = $this->Career_model->get_career_where($id_career);

		if(file_exists('uploads/images/careers/'.$image_file_name->career_image) && $image_file_name->career_image){
			@unlink('uploads/images/careers/'.$image_file_name->career_image);
		}

		if ($this->Career_model->delete_career_where($id_career)) {
			$response = ['success' => TRUE, 'message' => 'Data Promosi Berhasil Dihapus'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function store_career()
	{
		$this->_validate_career();

		$data = [
			'judul_career' => $this->input->post('judul_career'),
			'career_job_position_id' => $this->input->post('career_job_position_id'),
			'career_job_area_id' => $this->input->post('career_job_area_id'),
			'slug' => slug($this->input->post('judul_career')),
			'career_qualification' => $this->input->post('career_qualification'),
		];

		if(!empty($_FILES['career_image']['name'])){
			$upload = $this->_do_upload();
			$data['career_image'] = $upload;
		}

		if ($this->Career_model->insert_career($data)) { 
			$response = ['status' => TRUE, 'message' => 'Data Halaman Career Berhasil Ditambah'];
			echo json_encode($response);
		}
	}

	public function career_update($id_career)
	{
		$this->_validate_career_update();

		$data = [
			'judul_career' => $this->input->post('judul_career'),
			'career_job_position_id' => $this->input->post('career_job_position_id'),
			'career_job_area_id' => $this->input->post('career_job_area_id'),
			'slug' => slug($this->input->post('judul_career')),
			'career_qualification' => $this->input->post('career_qualification'),
		];
		
		if(!empty($_FILES['career_image']['name'])){
			$upload = $this->_do_upload();

			$career_image = $this->Career_model->get_career_where($id_career);

			if (file_exists('./uploads/images/careers/'.$career_image->career_image)) {
				@unlink('./uploads/images/careers/'.$career_image->career_image);
			}

			$data['career_image'] =  $upload;

		}

		if ($this->Career_model->update_career($id_career, $data)) {
			$response = ['status' => TRUE, 'message' => 'Data Career Berhasil Diubah'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	private function _do_upload()
	{
		$config['upload_path']          = './uploads/images/careers/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 5120;
		$config['file_name']            = round(microtime(true) * 1000);

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload("career_image")){
			$data['inputerror'][] = 'career_image';
			$data['error_string'][] = 'Upload error : '.$this->upload->display_errors('','');
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}

		return $this->upload->data('file_name');
	}

	private function _validate_career()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul_career') == '')
		{
			$data['inputerror'][] = 'judul_career';
			$data['error_string'][] = 'Judul Career Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('career_job_position_id') == '')
		{
			$data['inputerror'][] = 'career_job_position_id';
			$data['error_string'][] = 'Job Position Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('career_job_area_id') == '')
		{
			$data['inputerror'][] = 'career_job_area_id';
			$data['error_string'][] = 'Job Area Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('career_qualification') == '')
		{
			$data['inputerror'][] = 'career_qualification';
			$data['error_string'][] = 'Job Qualifications Wajib Diisi';
			$data['status'] = FALSE;
		}

		if(empty($_FILES['career_image']['name']))
		{
			$data['inputerror'][] = 'career_image';
			$data['error_string'][] = 'Career Image Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_career_update()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('judul_career') == '')
		{
			$data['inputerror'][] = 'judul_career';
			$data['error_string'][] = 'Judul Career Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('career_job_position_id') == '')
		{
			$data['inputerror'][] = 'career_job_position_id';
			$data['error_string'][] = 'Job Position Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('career_job_area_id') == '')
		{
			$data['inputerror'][] = 'career_job_area_id';
			$data['error_string'][] = 'Job Area Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('career_qualification') == '')
		{
			$data['inputerror'][] = 'career_qualification';
			$data['error_string'][] = 'Job Qualifications Wajib Diisi';
			$data['status'] = FALSE;
		}
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	/**
	 * [job_position description]
	 * @return [type] [description]
	 */
	public function job_position()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Halaman Career - Job Position',
			'content' => $this->load->view('admin/pages/career/content_career_job_position_view', [
				'career_job_positions' => $this->Career_model->get_career_job_positions(),
				'career_job_areas' => $this->Career_model->get_career_job_areas(),

			], TRUE),

			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function store_job_position()
	{
		$this->_validate_job_position();

		$data = $this->input->post();

		if($this->Career_model->insert_career_job_posision($data)){
			$response = ['success'=> TRUE, 'message' => 'Data Job Position Berhasil Dibuat'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function jobposition_edit($id_job_position)
	{
		$job_position = $this->Career_model->get_career_job_position_where($id_job_position);

		if($job_position){
			$response = ['success' => TRUE, 'data' => $job_position ];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function jobposition_update($id_job_position)
	{
		$this->_validate_job_position();

		$data = $this->input->post();

		if($this->Career_model->update_career_job_posision($id_job_position, $data)){
			$response = ['success'=> TRUE, 'message' => 'Data Job Position Berhasil Diubah'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function jobposition_delete($id_job_position)
	{
		if($this->Career_model->delete_career_job_posision_where($id_job_position)){
			$response = ['success' => TRUE, 'message' => 'Data Job Position Berhasil Dihapus'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	private function _validate_job_position()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name_career_job_position') == '')
		{
			$data['inputerror'][] = 'name_career_job_position';
			$data['error_string'][] = 'Nama Job Position Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function job_area()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Halaman Career - Job Area',
			'content' => $this->load->view('admin/pages/career/content_career_job_area_view', [
				'career_job_areas' => $this->Career_model->get_career_job_areas(),

			], TRUE),

			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function store_job_area()
	{
		$this->_validate_job_area();

		$data = $this->input->post();

		if($this->Career_model->insert_career_job_area($data)){
			$response = ['success'=> TRUE, 'message' => 'Data Job Area Berhasil Dibuat'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function job_area_edit($id_job_area)
	{
		$job_area = $this->Career_model->get_career_job_area_where($id_job_area);

		if($job_area){
			$response = ['success' => TRUE, 'data' => $job_area ];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function job_area_update($id_job_area)
	{
		$this->_validate_job_area();

		$data = $this->input->post();

		if($this->Career_model->update_career_job_area($id_job_area, $data)){
			$response = ['success'=> TRUE, 'message' => 'Data Job Area Berhasil Diubah'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function job_area_delete($id_job_area)
	{
		if($this->Career_model->delete_career_job_area_where($id_job_area)){
			$response = ['success' => TRUE, 'message' => 'Data Job Area Berhasil Dihapus'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	private function _validate_job_area()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name_career_job_area') == '')
		{
			$data['inputerror'][] = 'name_career_job_area';
			$data['error_string'][] = 'Nama Job Area Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	/**
	 * CAREER REGISTER
	 */
	public function career_index()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Career',
			'content' => $this->load->view('admin/pages/career/content_register_career_view', [
				'career_registers' => $this->Career_model->get_career_registers(),
			], TRUE),
			
			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function career_register_delete($id_career_register)
	{
		$cv_career = $this->Career_model->get_career_register_where($id_career_register);

		if(file_exists('uploads/careers/cvs/'.$cv_career->cv_career_register) && $cv_career->cv_career_register){
			@unlink('uploads/careers/cvs/'.$cv_career->cv_career_register);
		}

		if ($this->Career_model->delete_career_register_where($id_career_register)) {
			$response = ['success' => TRUE, 'message' => 'Data Career Register Berhasil Dihapus'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function download($id_career_register)
	{
		if (!empty($id_career_register)) {

			$cv = $this->Career_model->get_career_register_where($id_career_register);

			$file = 'uploads/careers/cvs/'.$cv->cv_career_register;

			force_download($file, NULL);
		}
		
	}

}

/* End of file Career.php */
/* Location: ./application/controllers/admin/Career.php */