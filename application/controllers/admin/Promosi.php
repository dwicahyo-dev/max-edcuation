<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promosi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->login_checker->check_login_admin();
	}

	public function index()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Absensi Siswa',
			'content' => $this->load->view('admin/pages/promosi/content_promosi_view', [
				'promosis' => $this->Promosi_model->get_promosis(),
			], TRUE),
			
			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function tambah_halaman_promosi()
	{
		$data = [
			'title' => 'MAX Education | Admin - Tambah Data Halaman Promosi',
			'content' => $this->load->view('admin/pages/promosi/content_tambah_halaman_promosi_view', TRUE, TRUE),

			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),

		];

		$this->load->view('admin/index', $data);
	}

	public function edit_halaman_promosi($id_halaman_promosi)
	{
		$data = [
			'title' => 'MAX Education | Admin - Tambah Data Halaman Promosi',
			'content' => $this->load->view('admin/pages/promosi/content_edit_halaman_promosi_view', [
				'promosi' => $this->Promosi_model->get_promosi_where($id_halaman_promosi),
			], TRUE),

			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),

		];

		$this->load->view('admin/index', $data);
	}

	public function store_promosi()
	{
		// $this->_validate();

		$data = [
			'nama_halaman_promosi' => $this->input->post('nama_halaman_promosi'),
			'slug' => slug($this->input->post('nama_halaman_promosi')),
			'isi_halaman_promosi' => $this->input->post('isi_halaman_promosi'),

		];

		if(!empty($_FILES['image_halaman_promosi']['name'])){
			$upload = $this->_do_upload();
			$data['image_halaman_promosi'] = $upload;
		}

		if ($this->Promosi_model->insert_halaman_promosi($data)) { 
			$response = ['status' => TRUE, 'message' => 'Data Halaman Promosi Berhasil Ditambah'];
			echo json_encode($response);
		}
	}

	public function halaman_promosi_update($id_halaman_promosi)
	{
		// $this->_validate();
		
		$data = [
			'nama_halaman_promosi' => $this->input->post('nama_halaman_promosi'),
			'slug' => slug($this->input->post('nama_halaman_promosi')),
			'isi_halaman_promosi' => $this->input->post('isi_halaman_promosi'),

		];

		if(!empty($_FILES['image_halaman_promosi']['name'])){
			$upload = $this->_do_upload();

			$image_halaman_promosi = $this->Promosi_model->get_promosi_where($id_halaman_promosi);

			if (file_exists('./uploads/images/promosis/'.$image_halaman_promosi->image_halaman_promosi)) {
				@unlink('./uploads/images/promosis/'.$image_halaman_promosi->image_halaman_promosi);
			}

			$data['image_halaman_promosi'] =  $upload;

		}

		if ($this->Promosi_model->update_promosi($id_halaman_promosi, $data)) {
			$response = ['status' => TRUE, 'message' => 'Data Promosi Berhasil Diubah'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function delete_promosi($id_halaman_promosi)
	{
		$image_file_name = $this->Promosi_model->get_promosi_where($id_halaman_promosi);

		if(file_exists('uploads/images/promosis/'.$image_file_name->image_halaman_promosi) && $image_file_name->image_halaman_promosi){
			@unlink('uploads/images/promosis/'.$image_file_name->image_halaman_promosi);
		}

		if ($this->Promosi_model->delete_promosi_where($id_halaman_promosi)) {
			$response = ['success' => TRUE, 'message' => 'Data Promosi Berhasil Dihapus'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}

	}

	public function siswa_promosi()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Siswa Promosi',
			'content' => $this->load->view('admin/pages/promosi/content_siswa_promosi_view', [
				'siswa_promosis' => $this->Promosi_model->get_siswa_promosis(),
			], TRUE),
			
			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	private function _do_upload()
	{
		$config['upload_path']          = './uploads/images/promosis/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 1000;
		$config['file_name']            = round(microtime(true) * 1000);

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload("image_halaman_promosi")){
			$data['inputerror'][] = 'image_halaman_promosi';
			$data['error_string'][] = 'Upload error : '.$this->upload->display_errors('','');
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_halaman_promosi') == '')
		{
			$data['inputerror'][] = 'nama_halaman_promosi';
			$data['error_string'][] = 'Nama Halaman Promosi Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('isi_halaman_promosi') == '')
		{
			$data['inputerror'][] = 'isi_halaman_promosi';
			$data['error_string'][] = 'Isi Halaman Promosi Wajib Diisi';
			$data['status'] = FALSE;
		}

		// if(empty($_FILES['image_slideshow']['name']))
		// {
		// 	$data['inputerror'][] = 'image_slideshow';
		// 	$data['error_string'][] = 'Image Slideshow Wajib Diisi';
		// 	$data['status'] = FALSE;
		// }

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}



}

/* End of file Promosi.php */
/* Location: ./application/controllers/admin/Promosi.php */