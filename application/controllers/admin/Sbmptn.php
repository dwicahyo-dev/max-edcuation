<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sbmptn extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->login_checker->check_login_admin();
	}

	public function index()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Siswa SMA',
			'content' => $this->load->view('admin/pages/sbmptn/content', [
				'siswa_sbmptn' => $this->Siswa_model->get_siswa_by_program(4),
			], TRUE),

			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function detail_siswa($id_siswa)
	{
		$siswa = $this->Siswa_model->get_siswa_where($id_siswa);
		$this->output->set_content_type('application/json')->set_output(json_encode($siswa));
	}

}

/* End of file Sd.php */
/* Location: ./application/controllers/admin/pages/Sd.php */