<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_program extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->login_checker->check_login_admin();
	}

	public function index()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Paket Pada Setiap Program',
			'content' => $this->load->view('admin/pages/paket_program/content_paket_program_view', [
				'paket_programs' => $this->Paket_program_model->get_paket_programs(),
				'programs' => $this->Program_model->get_program(),
				'pakets' => $this->Paket_model->get_pakets(),
			], TRUE),
			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function paket_program_create()
	{
		$this->_validate_paket_program();

		$data = $this->input->post();

		if($this->Paket_program_model->insert_paket_program($data)){
			$response = ['success'=> TRUE, 'message' => 'Data Paket Berhasil Dibuat'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function paket_program_update($id_paket_program)
	{
		$this->_validate_paket_program();

		$data = $this->input->post();

		if($this->Paket_program_model->update_paket_program($id_paket_program, $data)){
			$response = ['success'=> TRUE, 'message' => 'Data Paket Berhasil Diubah'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function paket_program_edit($id_paket_program)
	{	
		$paket = $this->Paket_program_model->get_paket_program_where($id_paket_program);

		if($paket){
			$response = ['success' => TRUE, 'data' => $paket ];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}else{
			$this->output->set_content_type('application/json')->set_output(
				json_encode(
					['success' => FALSE, 'error' => 'Data Paket Gagal Dimuat']
				)
			);
		}
		
	}

	public function paket_program_delete($id_paket_program)
	{
		if($this->Paket_program_model->paket_program_delete($id_paket_program)){
			$response = ['success' => TRUE, 'message' => 'Data Paket Berhasil Dihapus'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	private function _validate_paket_program()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('program_id') == '')
		{
			$data['inputerror'][] = 'program_id';
			$data['error_string'][] = 'Nama Program Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('paket_id') == '')
		{
			$data['inputerror'][] = 'paket_id';
			$data['error_string'][] = 'Nama Paket Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('harga_paket') == '')
		{
			$data['inputerror'][] = 'harga_paket';
			$data['error_string'][] = 'Harga Paket Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('waktu_belajar') == '')
		{
			$data['inputerror'][] = 'waktu_belajar';
			$data['error_string'][] = 'Jumlah Belajar Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('jumlah_pertemuan') == '')
		{
			$data['inputerror'][] = 'jumlah_pertemuan';
			$data['error_string'][] = 'Jumlah Pertemuan Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}

/* End of file Paket_program.php */
/* Location: ./application/controllers/admin/Paket_program.php */