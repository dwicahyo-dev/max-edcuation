<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->login_checker->check_login_admin();
	}

	public function index()
	{
		$data = [
			'title' => 'MAX Education | Admin - Data Paket',
			'content' => $this->load->view('admin/pages/paket/content_paket_view', [
				'pakets' => $this->Paket_model->get_pakets(),
			], TRUE),
			'user_admin' => $this->Admin_model->get_admin_by_id($this->session->userdata('admin_id')),
			'user' => $this->User_model->get_user_by_id($this->session->userdata('user_id')),
		];

		$this->load->view('admin/index', $data);
	}

	public function paket_create()
	{
		$this->_validate_paket();

		$data = $this->input->post();

		if($this->Paket_model->insert_paket($data)){
			$response = ['success'=> TRUE, 'message' => 'Data Paket Berhasil Dibuat'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function paket_update($id_paket)
	{
		$this->_validate_paket();

		$data = $this->input->post();

		if($this->Paket_model->update_paket($id_paket, $data)){
			$response = ['success'=> TRUE, 'message' => 'Data Paket Berhasil Diubah'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function paket_edit($id_paket)
	{	
		$paket = $this->Paket_model->get_paket_where($id_paket);

		if($paket){
			$response = ['success' => TRUE, 'data' => $paket ];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}else{
			$this->output->set_content_type('application/json')->set_output(
				json_encode(
					['success' => FALSE, 'error' => 'Data Paket Gagal Dimuat']
				)
			);
		}
		
	}

	public function paket_delete($id_paket)
	{
		if($this->Paket_model->paket_delete($id_paket)){
			$response = ['success' => TRUE, 'message' => 'Data Paket Berhasil Dihapus'];
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	private function _validate_paket()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_paket') == '')
		{
			$data['inputerror'][] = 'nama_paket';
			$data['error_string'][] = 'Nama Paket Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}

/* End of file Paket.php */
/* Location: ./application/controllers/admin/Paket.php */