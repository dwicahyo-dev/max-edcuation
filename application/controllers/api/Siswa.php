<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// header("Access-Control-Allow-Origin: *");

require_once APPPATH . 'controllers/api/Restdata.php';

class Siswa extends Restdata {

	public function form_pendaftaran_siswa_post()
	{
		$data = [
			'nama_lengkap_siswa' => $this->post('nama_lengkap_siswa', TRUE),
			'jenis_kelamin_id' => $this->post('id_jenis_kelamin', TRUE),
			'nomor_hp_siswa' => $this->post('nomor_hp_siswa', TRUE),
			'nomor_hp_orang_tua' => $this->post('nomor_hp_orang_tua', TRUE),
			'alamat_lengkap' => $this->post('alamat_lengkap', TRUE),
			'domisili_id' => $this->post('id_domisili', TRUE),
			'tingkat_sekolah_id' => $this->post('id_tingkat_sekolah', TRUE),
			'program_id' => $this->post('id_program', TRUE),
			'kelas' => $this->post('kelas', TRUE),
		];

		$this->form_validation->set_rules('nama_lengkap_siswa','Nama Lengkap','trim|max_length[255]|required', ['required' => 'Nama Lengkap Wajib Diisi']);
		$this->form_validation->set_rules('id_jenis_kelamin', 'Jenis Kelamin', 'required', ['required' => 'Jenis Kelamin Wajib Diisi']);
		$this->form_validation->set_rules('nomor_hp_siswa','Nomor HP Siswa','trim|required|max_length[15]', ['required' => 'Nomor HP Siswa Wajib Diisi']);
		$this->form_validation->set_rules('nomor_hp_orang_tua','Nomor HP Orang Tua','trim|required|max_length[255]', ['required' => 'Nomor HP Orang Tua Wajib Diisi']);
		$this->form_validation->set_rules('alamat_lengkap','Alamat Lengkap','trim|required|max_length[255]', ['required' => 'Alamat Lengkap Wajib Diisi']);
		$this->form_validation->set_rules('id_domisili', 'Domisili', 'required', ['required' => 'Domisili Wajib Diisi']);
		$this->form_validation->set_rules('id_tingkat_sekolah', 'Tingkat Sekolah', 'required', ['required' => 'Tingkat Sekolah Wajib Diisi']);

		$this->form_validation->set_rules('kelas','Kelas','trim|required|max_length[255]', ['required' => 'Kelas Wajib Diisi']);
		// $this->form_validation->set_rules('nama_sekolah','Nama Sekolah','trim|required|max_length[255]', ['required' => 'Nama Sekolah Wajib Diisi']);
		$this->form_validation->set_rules('mata_pelajaran[]','Mata Pelajaran','required', ['required' => 'Mata Pelajaran Wajib Diisi']);
		$this->form_validation->set_rules('hari[]','Hari Les','required', ['required' => 'Hari Les Wajib Diisi']);
		$this->form_validation->set_rules('jam[]','Jam Les','required', ['required' => 'Jam Les Wajib Diisi']);

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.maxeducation.id',
			'smtp_port' => 465,
			'smtp_user' => 'admin@maxeducation.id',
			'smtp_pass' => 'maxedu2017',
			'mailtype'  => 'html', 
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
			'smtp_crypto' => 'ssl',
			'newline' => '\r\n',
		);
		
		$this->load->library('email',$config);
		$this->email->from('admin@maxeducation.id', 'Recruitment MAX Education');
		$this->email->to('maxeducationindonesia@gmail.com');
		
		$this->email->subject('New Siswa have signed up');

		if ($this->form_validation->run() == FALSE) {
			$this->badreq($this->validation_errors());
		}else {
			$mata_pelajaran = $this->post('mata_pelajaran');

			$id_siswa = $this->Siswa_model->register_siswa($data);

			/**
			 * [$result_mata_pelajaran description]
			 * @var array
			 */
			$result_mata_pelajaran = array();
			foreach((array) $mata_pelajaran as $key => $val){
				$result_mata_pelajaran[] = array(
					"siswa_id"  => $id_siswa,
					"mata_pelajaran_id"  => $_POST['mata_pelajaran'][$key],
				);
			}

			/**
		 	* [hari Detail Siswa Hari]
		 	*/
		 	$hari = $this->post('hari');
		 	$result_hari = array();
		 	foreach((array) $hari AS $key => $val){
		 		$result_hari[] = array(
		 			"siswa_id"  => $id_siswa,
		 			"hari_id"  => $_POST['hari'][$key],
		 		);
		 	}

			/**
			 * [jam Detail Siswa Jam]
			 */
			$jam = $this->post('jam');
			$result_jam = array();
			foreach((array) $jam AS $key => $val){
				$result_jam[] = array(
					"siswa_id"  => $id_siswa,
					"jam_id"  => $_POST['jam'][$key],
				);
			}

			$this->db->insert_batch('siswa_detail_mata_pelajaran', $result_mata_pelajaran);
			$this->db->insert_batch('siswa_detail_hari', $result_hari);
			$this->db->insert_batch('siswa_detail_jam', $result_jam);
			
			$jenis_kelamin = $this->Jenis_kelamin_model->get_jenis_kelamin_where($data['jenis_kelamin_id']);
			$domisili = $this->Domisili_model->get_domisili_where($data['domisili_id']);
			$tingkat_sekolah = $this->Tingkat_sekolah_model->get_tingkat_sekolah_where($data['tingkat_sekolah_id']);
			$program = $this->Program_model->get_program_where($data['program_id']);
			

			$message .= 'Nama Siswa : '. $data['nama_lengkap_siswa']."<br>";
			$message .= 'Jenis Kelamin : '. $jenis_kelamin->jenis_kelamin."<br>";
			$message .= 'Nomor HP Siswa : '. $data['nomor_hp_siswa']."<br>";
			$message .= 'Nomor HP Orang Tua : '. $data['nomor_hp_orang_tua']."<br>";
			$message .= 'Alamat : '. $data['alamat_lengkap']."<br>";
			$message .= 'Domisili : '. $domisili->domisili."<br>";
			$message .= 'Tingkat Sekolah : '. $tingkat_sekolah->tingkat_sekolah."<br>";
			$message .= 'Kelas : '. $data['kelas']."<br>";
			$message .= 'Program : '. $program->program."<br>";
			
			$this->email->message($message);
			$this->email->send();

			$this->response($data, Restdata::HTTP_CREATED);
		}
	}

	public function belajar_gratis_post()
	{
		$data = [
			'nama_lengkap_siswa' => $this->post('nama_lengkap_siswa', TRUE),
			'gelombang' => $this->post('gelombang', TRUE),
			'nama_ig' => $this->post('nama_ig', TRUE),
		];

		$this->form_validation->set_rules('nama_lengkap_siswa','Nama Lengkap','trim|max_length[255]|required', ['required' => 'Nama Wajib Diisi']);
		$this->form_validation->set_rules('nama_ig','Nama IG','trim|max_length[255]|required', ['required' => 'Nama IG Wajib Diisi']);
		
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.maxeducation.id',
			'smtp_port' => 465,
			'smtp_user' => 'admin@maxeducation.id',
			'smtp_pass' => 'maxedu2017',
			'mailtype'  => 'html', 
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
			'smtp_crypto' => 'ssl',
			'newline' => '\r\n',
		);
		
		$this->load->library('email',$config);
		$this->email->from('admin@maxeducation.id', 'Recruitment MAX Education');
		$this->email->to('maxeducationindonesia@gmail.com');
		
		$this->email->subject('New Siswa Belajar Gratis have signed up');

		if ($this->form_validation->run() == FALSE) {
			$this->badreq($this->validation_errors());
		}else {
		    $this->Siswa_model->register_siswa_belajar_gratis($data);
		    
		    $message .= 'Nama Siswa : '. $data['nama_lengkap_siswa']."<br>";
			$message .= 'Gelombang : '. $data['gelombang']."<br>";
			$message .= 'Nama Instagram : '. $data['nama_ig']."<br>";
			
			$this->email->message($message);
			$this->email->send();
		
			
			$this->response($data, Restdata::HTTP_CREATED);
			
		}
	}

	public function siswa_promosi_post()
	{
		$data = [
			'nama_lengkap_siswa_promosi' => $this->post('nama_lengkap_siswa_promosi', TRUE),
			'jenis_kelamin_id' => $this->post('jenis_kelamin_id', TRUE),
			'email_siswa_promosi' => $this->post('email_siswa_promosi', TRUE),
			'alamat_lengkap_siswa_promosi' => $this->post('alamat_lengkap_siswa_promosi', TRUE),
			'nomor_hp_siswa_promosi' => $this->post('nomor_hp_siswa_promosi', TRUE),
			'halaman_promosi_id' => $this->post('halaman_promosi_id', TRUE),
		];

		$this->form_validation->set_rules('nama_lengkap_siswa_promosi','Nama Lengkap','trim|max_length[255]|required', ['required' => 'Nama Lengkap Wajib Diisi']);
		$this->form_validation->set_rules('jenis_kelamin_id','Jenis Kelamin','trim|max_length[255]|required', ['required' => 'Jenis Kelamin Wajib Diisi']);
		$this->form_validation->set_rules('email_siswa_promosi','Email','trim|required|max_length[255]', ['required' => 'Email Wajib Diisi']);
		$this->form_validation->set_rules('alamat_lengkap_siswa_promosi','Alamat Lengkap','trim|required|max_length[255]', ['required' => 'Alamat Lengkap Wajib Diisi']);
		$this->form_validation->set_rules('nomor_hp_siswa_promosi','Nomor HP Siswa','trim|required|max_length[15]', ['required' => 'Nomor HP Wajib Diisi']);
		
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.maxeducation.id',
			'smtp_port' => 465,
			'smtp_user' => 'admin@maxeducation.id',
			'smtp_pass' => 'maxedu2017',
			'mailtype'  => 'html', 
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
			'smtp_crypto' => 'ssl',
			'newline' => '\r\n',
		);
		
		$this->load->library('email',$config);
		$this->email->from('admin@maxeducation.id', 'Recruitment MAX Education');
		$this->email->to('maxeducationindonesia@gmail.com');
		
		$this->email->subject('New Siswa Promosi have signed up');

		if ($this->form_validation->run() == FALSE) {
			$this->badreq($this->validation_errors());
		}else {
		    $this->Promosi_model->insert_siswa_promosi($data);
		    
		    $promosi = $this->Promosi_model->get_promosi_where($data['halaman_promosi_id']);
			$jenis_kelamin = $this->Jenis_kelamin_model->get_jenis_kelamin_where($data['jenis_kelamin_id']);


			$message  .= 'Nama Siswa : '. $data['nama_lengkap_siswa_promosi']."<br>";
			$message  .= 'Jenis Kelamin : '. $jenis_kelamin->jenis_kelamin."<br>";
			$message  .= 'Email : '. $data['email_siswa_promosi']."<br>";
			$message  .= 'Alamat : '. $data['alamat_lengkap_siswa_promosi']."<br>";
			$message  .= 'Nomor HP Siswa : '. $data['nomor_hp_siswa_promosi']."<br>";
			$message  .= 'Mendaftar Pada Halaman Promosi : '. $promosi->nama_halaman_promosi."<br>";
		    
		    $this->email->message($message);
			$this->email->send();
		    
			
			$this->response($data, Restdata::HTTP_CREATED);
			
		}
	}

}

/* End of file Siswa.php */
/* Location: ./application/controllers/api/Siswa.php */