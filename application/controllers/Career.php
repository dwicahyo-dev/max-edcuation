<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = [
			'title' => 'MAX Education | Career',
			'content' => $this->load->view('program/career/content_halaman_career_view', [
				'careers' => $this->Career_model->get_careers(),
			], TRUE),
		];

		$this->load->view('template/index', $data);
	}

	public function career_by_slug($slug)
	{
		$career = $this->Career_model->get_career_by_slug($slug);

		$data = [
			'title' => 'MAX Education - Career | '. $career->judul_career,
			'content' => $this->load->view('program/career/content_career_detail_view', [
				'career' => $this->Career_model->get_career_by_slug($slug),
			], TRUE),
		];

		$this->load->view('template/index', $data);
	}

	public function store_career_register()
	{
		$this->_validate_career_register();

		$data = [
			'name_career_register' => $this->input->post('name_career_register'),
			'career_id' => $this->input->post('career_id'),
		];

		if(!empty($_FILES['cv_career_register']['name'])){
			$upload = $this->_do_upload($data['name_career_register']);
			$data['cv_career_register'] = $upload;
		}

		if ($this->Career_model->insert_career_register($data)) { 
			$response = ['status' => TRUE, 'message' => 'Berhasil Apply Job dan Akan segera diproses. Admin akan menghubungi kamu!'];
			echo json_encode($response);
		}
	}

	private function _do_upload($name_career_register)
	{
		$config['upload_path']          = './uploads/careers/cvs/';
		$config['allowed_types']        = 'rar|zip|pdf|doc|docx';
		$config['max_size']             = 5120;
		$config['file_name']            = $name_career_register. '_CV_'. round(microtime(true) * 1000);

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload("cv_career_register")){
			$data['inputerror'][] = 'cv_career_register';
			$data['error_string'][] = 'Upload error : '.$this->upload->display_errors('','');
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}

		return $this->upload->data('file_name');
	}

	private function _validate_career_register()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('name_career_register') == '')
		{
			$data['inputerror'][] = 'name_career_register';
			$data['error_string'][] = 'Nama Lengkap Wajib Diisi';
			$data['status'] = FALSE;
		}

		if(empty($_FILES['cv_career_register']['name']))
		{
			$data['inputerror'][] = 'cv_career_register';
			$data['error_string'][] = 'CV Wajib Diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}

/* End of file Career.php */
/* Location: ./application/controllers/Career.php */