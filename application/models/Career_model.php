<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career_model extends CI_Model {
	private $table = 'career'; 
	private $table_job_position = 'career_job_position';
	private $table_job_area = 'career_job_area';
	private $table_career_register = 'career_register';


	/**
	 * [CAREERS ]
	 * @return [type]
	 */
	public function get_careers()
	{
		return $this->db->from($this->table)
		 				->join('career_job_position', 
								'career_job_position.id_career_job_position = career.career_job_position_id')
						->join('career_job_area', 
								'career_job_area.id_career_job_area = career.career_job_area_id')
                        ->get()->result();
	}

	public function get_career_where($id_career)
	{
		return $this->db->from($this->table)
						->where('id_career', $id_career)
						->join('career_job_position', 
								'career_job_position.id_career_job_position = career.career_job_position_id')
						->join('career_job_area', 
								'career_job_area.id_career_job_area = career.career_job_area_id')
						->get()->row();
	}

	public function get_career_by_slug($slug)
	{
		return $this->db->from($this->table)
						->where('slug', $slug)
						->join('career_job_position', 
								'career_job_position.id_career_job_position = career.career_job_position_id')
						->join('career_job_area', 
								'career_job_area.id_career_job_area = career.career_job_area_id')
						->get()->row();
	}

	public function insert_career($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function update_career($id_career, $data)
	{
		return $this->db->where('id_career', $id_career)
                        ->update($this->table, $data);
	}

	public function delete_career_where($id_career)
	{
		return $this->db->where('id_career', $id_career)
						->delete($this->table);
	}

	/**
	 * [CAREERS - JOB POSITION ]
	 */
	public function get_career_job_positions()
	{
		return $this->db->from($this->table_job_position)
                        ->get()->result();
	}

	public function get_career_job_position_where($id_career_job_position)
	{
		return $this->db->from($this->table_job_position)
						->where('id_career_job_position', $id_career_job_position)
						->get()->row();
	}

	public function insert_career_job_posision($data)
	{
		return $this->db->insert($this->table_job_position, $data);
	}

	public function update_career_job_posision($id_career_job_position, $data)
	{
		return $this->db->where('id_career_job_position', $id_career_job_position)
                        ->update($this->table_job_position, $data);
	}

	public function delete_career_job_posision_where($id_career_job_position)
	{
		return $this->db->where('id_career_job_position', $id_career_job_position)
						->delete($this->table_job_position);
	}

	/**
	 * [CAREERS - JOB AREA ]
	 */
	public function get_career_job_areas()
	{
		return $this->db->from($this->table_job_area)
                        ->get()->result();
	}

	public function get_career_job_area_where($id_career_job_area)
	{
		return $this->db->from($this->table_job_area)
						->where('id_career_job_area', $id_career_job_area)
						->get()->row();
	}

	public function insert_career_job_area($data)
	{
		return $this->db->insert($this->table_job_area, $data);
	}

	public function update_career_job_area($id_career_job_area, $data)
	{
		return $this->db->from($this->table_job_area)
						->where('id_career_job_area', $id_career_job_area)
                        ->update($this->table_job_area, $data);
	}

	public function delete_career_job_area_where($id_career_job_area)
	{
		return $this->db->where('id_career_job_area', $id_career_job_area)
						->delete($this->table_job_area);
	}

	/**
	 * CAREER REGISTER
	 */
	public function get_career_registers()
	{
		return $this->db->from($this->table_career_register)
						->join('career',
								'career.id_career = career_register.career_id')
                        ->get()->result();
	}

	public function delete_career_register_where($id_career_register)
	{
		return $this->db->where('id_career_register', $id_career_register)
						->delete($this->table_career_register);
	}

	public function get_career_register_where($id_career_register)
	{
		return $this->db->from($this->table_career_register)
						->where('id_career_register', $id_career_register)
						->get()->row();
	}

	public function insert_career_register($data)
	{
		return $this->db->insert($this->table_career_register, $data);
	}


}

/* End of file Career_model.php */
/* Location: ./application/models/Career_model.php */