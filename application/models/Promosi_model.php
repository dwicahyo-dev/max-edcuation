<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promosi_model extends CI_Model {
	private $table = 'halaman_promosi';
	private $table_siswa_promosi = 'siswa_promosi';


	public function get_promosis()
	{
		return $this->db->from($this->table)
                        ->get()->result();
	}

	public function get_siswa_promosis()
	{
		return $this->db->select('siswa_promosi.*, jenis_kelamin.jenis_kelamin, halaman_promosi.nama_halaman_promosi')
						->from($this->table_siswa_promosi)
						->join('jenis_kelamin', 
								'jenis_kelamin.id_jenis_kelamin = siswa_promosi.jenis_kelamin_id')
						->join('halaman_promosi',
								'halaman_promosi.id_halaman_promosi = siswa_promosi.halaman_promosi_id')
                        ->get()->result();
	}

	public function insert_siswa_promosi($data)
	{
		return $this->db->insert($this->table_siswa_promosi, $data);
	}

	public function update_promosi($id_halaman_promosi, $data)
	{
		return $this->db->where('id_halaman_promosi', $id_halaman_promosi)
                        ->update($this->table, $data);
	}

	public function insert_halaman_promosi($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function get_promosi_where($id_halaman_promosi)
	{
		return $this->db->from($this->table)
						->where('id_halaman_promosi', $id_halaman_promosi)
						->get()->row();
	}

	public function delete_promosi_where($id_halaman_promosi)
	{
		return $this->db->where('id_halaman_promosi', $id_halaman_promosi)
						->delete($this->table);
	}

	public function get_promosi_by_slug($slug)
	{
		return $this->db->from($this->table)
						->where('slug', $slug)
						->get()->row();
	}

}

/* End of file Promosi_model.php */
/* Location: ./application/models/Promosi_model.php */