<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_program_detail_model extends CI_Model {
	private $table = 'paket_program_detail';

	public function get_paket_where($id_paket_program)
	{
		return $this->db->from($this->table)
						->where('paket_program_id', $id_paket_program)
				 		->get()->result();
	}
	

}

/* End of file Paket_program_detail_model.php */
/* Location: ./application/models/Paket_program_detail_model.php */