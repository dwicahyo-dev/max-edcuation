<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tingkat_sekolah_model extends CI_Model {

	private $table = 'tingkat_sekolah';

	public function get_all_tingkat_sekolah()
	{
		return $this->db->select('*')
				->from($this->table)
				->get()->result();
	}
	
	public function get_tingkat_sekolah_where($id_tingkat_sekolah)
	{
		return $this->db->from($this->table)
						->where('id_tingkat_sekolah', $id_tingkat_sekolah)
						->get()->row();
	}

}

/* End of file Tingkat_sekolah_model.php */
/* Location: ./application/models/Tingkat_sekolah_model.php */