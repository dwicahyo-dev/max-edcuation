<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_program_model extends CI_Model {
	private $table = 'paket_program';

	public function get_paket_programs()
	{
		return $this->db->from($this->table)
						->join('program', 
								'program.id_program = paket_program.program_id')
						->join('paket', 
								'paket.id_paket = paket_program.paket_id')
						->order_by('program.program', 'asc')
				 		->get()->result();
	}

    public function get_paket_program_by_id($id_program)
    {
        return $this->db->from($this->table)
                        ->join('program', 
                                'program.id_program = paket_program.program_id')
                        ->join('paket', 
                                'paket.id_paket = paket_program.paket_id')
                        ->where('program.id_program', $id_program)
                        ->get()->result();
    }

	public function insert_paket_program($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update_paket_program($id_paket_program, $data)
    {
        return $this->db->where('id_paket_program', $id_paket_program)
                        ->update($this->table, $data);
    }

    public function get_paket_program_where($id_paket_program)
    {
    	return $this->db->from($this->table)
                        ->where('id_paket_program', $id_paket_program)
                        ->get()->row();
    }

    public function paket_program_delete($id_paket_program)
    {
        return $this->db->where('id_paket_program', $id_paket_program)->delete($this->table);
    }

	

}

/* End of file Paket_program_model.php */
/* Location: ./application/models/Paket_program_model.php */