<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domisili_model extends CI_Model {
	private $table = 'domisili';

	public function get_all_domisili()
	{
		return $this->db->select('*')
				->from($this->table)
				->get()->result();
	}
	
	public function get_domisili_where($id_domisili)
	{
		return $this->db->from($this->table)
						->where('id_domisili', $id_domisili)
						->get()->row();
	}



	

}

/* End of file Domisili_model.php */
/* Location: ./application/models/Domisili_model.php */