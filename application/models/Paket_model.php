<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_model extends CI_Model {
	private $table = 'paket';

	public function get_pakets()
	{
		return $this->db->from($this->table)
				 		->get()->result();
	}

	public function insert_paket($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update_paket($id_paket, $data)
    {
        return $this->db->where('id_paket', $id_paket)
                        ->update($this->table, $data);
    }

    public function get_paket_where($id_paket)
    {
    	return $this->db->from($this->table)
                        ->where('id_paket', $id_paket)
                        ->get()->row();
    }

    public function paket_delete($id_paket)
    {
        return $this->db->where('id_paket', $id_paket)->delete($this->table);
    }



}

/* End of file Paket_model.php */
/* Location: ./application/models/Paket_model.php */