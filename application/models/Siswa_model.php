<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa_model extends CI_Model {
	private $table = 'siswa';
	private $table_siswa_gratis = 'siswa_belajar_gratis';

	public function __construct(){
		parent::__construct();
		
	}

	/**
     * GET ALL OF SISWA
     */
	public function get_siswas()
	{
		return $this->db->from($this->table)
					->join('jenis_kelamin',
						   'jenis_kelamin.id_jenis_kelamin = siswa.jenis_kelamin_id')
					// ->join('agama',
					// 	   'agama.id_agama = siswa.agama_id')
					->join('domisili',
						   'domisili.id_domisili = siswa.domisili_id')
					->join('tingkat_sekolah',
						   'tingkat_sekolah.id_tingkat_sekolah = siswa.tingkat_sekolah_id')
					->join('program',
						   'program.id_program = siswa.program_id')
					->get()->result();
	}


	/**
     * GET SISWA BY ID PROGRAM
     */
	public function get_siswa_by_program($id_program)
	{
		return $this->db->from($this->table)
						->join('jenis_kelamin',
    						   'jenis_kelamin.id_jenis_kelamin = siswa.jenis_kelamin_id')
						->join('domisili',
    						   'domisili.id_domisili = siswa.domisili_id')
						->where('siswa.program_id', $id_program)
						->order_by('registered', 'DESC')
						->get()->result();
	}


	/**
	* GET SISWA WHERE ID SISWA
	*/
	public function get_siswa_where($id_siswa)
	{
		return $this->db->from($this->table)
						->join('jenis_kelamin', 
							   'jenis_kelamin.id_jenis_kelamin = siswa.jenis_kelamin_id')
						// ->join('agama', 
						// 	   'agama.id_agama = siswa.agama_id')
						->join('domisili', 
							   'domisili.id_domisili = siswa.domisili_id')
						->join('tingkat_sekolah', 
							   'tingkat_sekolah.id_tingkat_sekolah = siswa.tingkat_sekolah_id')
						->join('program', 
							   'program.id_program = siswa.program_id')
						->where('siswa.id_siswa', $id_siswa)
						->get()->row();
	}

	/**
	 * REGISTER SISWA
	 */
	public function register_siswa($data)
	{   
		// return $this->db->insert($this->table, $data);
		$this->db->insert($this->table, $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}

	public function register_siswa_belajar_gratis($data)
	{   
		return $this->db->insert('siswa_belajar_gratis', $data);
	}


	/**
	 * Get Siswa by Belajar Gratis
	 */
	public function get_siswa_belajar_gratis()
	{
		return $this->db->from($this->table_siswa_gratis)
						->order_by('registered', 'DESC')
						->get()->result();
	}


	/**
	 * [get_mata_pelajaran_siswa_detail_where description]
	 * @param  [type] $id_siswa [description]
	 * @return [type]           [description]
	 */
	public function get_mata_pelajaran_siswa_detail_where($id_siswa)
	{
		return $this->db->from('siswa_detail_mata_pelajaran')
						->join('siswa',
							   'siswa.id_siswa = siswa_detail_mata_pelajaran.siswa_id')
						->join('mata_pelajaran',
							   'mata_pelajaran.id_mata_pelajaran = siswa_detail_mata_pelajaran.mata_pelajaran_id')
						->where('siswa_detail_mata_pelajaran.siswa_id', $id_siswa)
						->get()->result();
	}

	public function get_hari_siswa_detail_where($id_siswa)
	{
		return $this->db->from('siswa_detail_hari')
		->join('siswa',
			'siswa.id_siswa = siswa_detail_hari.siswa_id')
		->join('hari',
			'hari.id_hari = siswa_detail_hari.hari_id')
		->where('siswa_detail_hari.siswa_id', $id_siswa)
		->get()->result();
	}

	public function get_jam_siswa_detail_where($id_siswa)
	{
		return $this->db->from('siswa_detail_jam')
		->join('siswa',
			'siswa.id_siswa = siswa_detail_jam.siswa_id')
		->join('jam',
			'jam.id_jam = siswa_detail_jam.jam_id')
		->where('siswa_detail_jam.siswa_id', $id_siswa)
		->get()->result();
	}

	public function get_users_siswa()
	{
		return $this->db->select('siswa.id_siswa, 
			siswa.nama_lengkap_siswa, 
			jenis_kelamin.jenis_kelamin,
			users.username,
			program.program,
			role.id_role,
			role.role,
			users.password,
			users.id_user

			')
		->from($this->table)
		->join('jenis_kelamin', 
			'jenis_kelamin.id_jenis_kelamin = siswa.jenis_kelamin_id')
		// ->join('agama', 
		// 	'agama.id_agama = siswa.agama_id')
		->join('program', 
			'program.id_program = siswa.program_id')
		->join('users', 
			'users.siswa_id = siswa.id_siswa', 'left')
		->join('role',
			'role.id_role = users.role_id', 'left')
		->get()->result();
	}


	public function update_siswa_informasi($id_siswa, $data)
	{
		return $this->db->where('id_siswa', $id_siswa)
					->update($this->table, $data);
	}



}

/* End of file Siswa_model.php */
/* Location: ./application/models/Siswa_model.php */