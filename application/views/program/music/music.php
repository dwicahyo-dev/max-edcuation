<section class="header11 cid-qIoZib7Tzp mbr-fullscreen" id="header11-z">

    <div class="container align-left">
        <div class="media-container-column mbr-white col-md-12">
            <h3 class="mbr-section-subtitle py-3 mbr-fonts-style display-5">MUSIC</h3>
            <h1 class="mbr-section-title py-3 mbr-fonts-style display-1">Belajar Musik Menjadi Profesional bersama&nbsp;<strong>MAX Education</strong>.<br>
            </h1>
            <p class="mbr-text py-3 mbr-fonts-style display-5">Memiliki talenta bermain musik memang memberikan daya tarik tersendiri. <strong>Max Education</strong> adalah tempat yang tepat bagi Anak Anda
                dalam mengembangkan talenta musik tersebut. Mari manfaatkan masa sekarang ini dengan berlatih musik bersama <strong>Max Education!</strong></p>

            </div>
        </div>


    </section>

    <section class="cid-qIWFSobCMv" id="pricing-tables1-1k" style="background-image: url(<?= base_url() ?>assets/images/mbr-1558x1080.jpg);">



        <div class="mbr-overlay" style="opacity: 0.2; background-color: rgb(0, 0, 0);">
        </div>
        <div class="container">
            <div class="media-container-row">
                <?php foreach ($paket_programs_music as $paket_program_music): ?>

                    <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                        <div class="plan-header text-center pt-5">
                            <h3 class="plan-title mbr-fonts-style display-5">
                                <?= $paket_program_music->nama_paket ?>
                            </h3>
                            <div class="plan-price">
                                <span class="price-value mbr-fonts-style display-7">Rp.&nbsp;</span>
                                <span class="price-figure mbr-fonts-style display-5"><?= number_format($paket_program_music->harga_paket,2,",",".")  ?></span>
                                <small class="price-term mbr-fonts-style display-7">
                                per bulan</small>
                            </div>
                        </div>
                        <div class="plan-body pb-5">
                            <div class="plan-list align-center">
                                <ul class="list-group list-group-flush mbr-fonts-style display-7">
                                    <?php foreach ($mata_pelajarans_music as $mata_pelajaran_music): ?>
                                        <li class="list-group-item"><?= $mata_pelajaran_music->mata_pelajaran ?></li> 
                                    <?php endforeach ?>

                                    <li class="list-group-item">Waktu Belajar <?= $paket_program_music->waktu_belajar ?> Jam</li>
                                    <li class="list-group-item"><?= $paket_program_music->jumlah_pertemuan ?>x Pertemuan/Bulan</li>

                                </ul>
                            </div>
                            <div class="mbr-section-btn text-center pt-4">
                                <a class="btn btn-primary display-4" data-toggle="modal" data-target="#modal_Register">
                                    <span class="mbri-edit mbr-iconfont mbr-iconfont-btn"></span>
                                    DAFTAR
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <!-- Modal for Register -->
        <div class="modal fade" id="modal_Register" role="document" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Register Form</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form  method="POST" action="javascript:void(0);" id="form_Register">
                            <div class="form-group">
                                <label class="control-label">Nama Lengkap *</label>
                                <input type="text" name="nama_lengkap_siswa" placeholder="Nama Lengkap" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="hari">Jenis Kelamin *</label>
                                <div class="form-line">
                                    <select name="id_jenis_kelamin" class="form-control" id="jenis_kelamin">
                                        <?php foreach ($jenis_kelamin as $row): ?>
                                            <option value="<?= $row->id_jenis_kelamin ?>"><?= $row->jenis_kelamin ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Nomor HP Siswa *</label>
                                <input type="number" name="nomor_hp_siswa" placeholder="Nomor HP Siswa" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Nomor HP Orang Tua Murid</label>
                                <input type="number" name="nomor_hp_orang_tua" placeholder="Nomor HP Orang Tua Murid" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Alamat Lengkap *</label>
                                <div class="form-line">
                                    <textarea name="alamat_lengkap" rows="4" placeholder="Alamat Lengkap" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="domisili">Domisili *</label>
                                <div class="form-line">
                                    <select name="id_domisili" class="form-control" id="domisili">
                                        <?php foreach ($domisili as $row): ?>
                                            <option value="<?= $row->id_domisili ?>"><?= $row->domisili ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tingkat_sekolah">Tingkat Sekolah *</label>
                                <div class="form-line">
                                    <select name="id_tingkat_sekolah" class="form-control" id="tingkat_sekolah">
                                        <?php foreach ($tingkat_sekolah as $row): ?>
                                            <option value="<?= $row->id_tingkat_sekolah ?>"><?= $row->tingkat_sekolah ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Kelas *</label>
                                <input type="text" name="kelas" placeholder="Kelas" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="mata_pelajaran">Mata Pelajaran *</label>
                                <div class="form-line">
                                    <select style="width: 100%; " class="form-control"  name="mata_pelajaran[]" multiple="" id="mata_pelajaran" >
                                        <?php foreach ($mata_pelajaran as $row): ?>
                                            <option value="<?= $row->id_mata_pelajaran ?>"><?= $row->mata_pelajaran ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mata_pelajaran">Hari *</label>
                                <div class="form-line">
                                    <select style="width: 100%; " class="form-control"  name="hari[]" multiple="" id="hari" >
                                        <?php foreach ($hari as $row): ?>
                                            <option value="<?= $row->id_hari ?>"><?= $row->hari ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mata_pelajaran">Jam *</label>
                                <div class="form-line">
                                    <select style="width: 100%; " class="form-control"  name="jam[]" multiple="" id="jam" >
                                        <?php foreach ($jam as $row): ?>
                                            <option value="<?= $row->id_jam ?>"><?= $row->jam. ' WIB' ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Durasi</label>
                                <input type="text" placeholder="Durasi" class="form-control" value="2 Jam" disabled="">
                            </div>

                            <input type="hidden" name="id_program" value="6">

                            <div class="form-group">
                                <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                                <label for="terms">I Agree to the <strong>Terms of Use</strong> and <strong>Privacy Policy</strong></label>
                            </div>

                            <button type="submit" id="btnRegister" class="btn btn-primary display-4"><span class="mbri-edit mbr-iconfont mbr-iconfont-btn" style="font-size: 20px;"></span>Register</button>
                            <button type="button" class="btn btn-danger display-4" data-dismiss="modal"><span class="mbri-close mbr-iconfont mbr-iconfont-btn" style="font-size: 20px;"></span>Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function() {
            $('#btnOnRegisterPrivate1').click(function() {
                $('#form_Register')[0].reset(); 
                $('#modal_Register').modal({backdrop: 'static', keyboard: false, show: true});
            });

        });

    </script>