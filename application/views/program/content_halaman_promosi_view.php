<style>
#sectionBelajarGratis{
	background-color: #eeeeee;
	/*background: url(<?= base_url() ?>assets/images/belajar_gratis_background.jpeg);*/

	margin-top: 77px;
	min-height:400px;

	background-position:center;
	background-size:cover;
	background-repeat:no-repeat;
	background-attachment:fixed;

}

.header{
	text-align: center;
	color: black;
	margin-top: 5%;
	margin-bottom: 5%;
}

#formPromosi {
	color: black;
	padding-top: 10%
}

.forSMP {
	margin-top: 2%;

	box-sizing: content-box;
	background-color: white;

	color: white;
	padding: 20px;

	border-radius: 10px;
	border-color: coral;
}

#btnOnKirim {
	margin-left: 26%;
}

.isi_halaman_promosi {
	color: black;


}

@media(max-width:568px){
	#sectionBelajarGratis{
		background-attachment:fixed;
	}

	.keterangan1 {
		margin-top: 10%;
	}

	.keterangan2 {
		margin-top: 10%;
	}

	.keterangancaranya {
		margin-top: 10%;
	}

	.button{
		margin-top: 10%;
		margin-left: 18%;
	}

/*	.forSMP {
		margin-top: 2%;

		box-sizing: content-box;
		background-color: white;

		color: white;
		padding: 20px;

		border-radius: 10px;
		}*/

		.forSMP > h1 {
			color: black;
			text-align: center;

		}
	}


</style>

<section id="sectionBelajarGratis" class="features3 cid-qInDyo8xet" >

	<div class="container">
		<h2 class="header" ><?= $halaman_promosi->nama_halaman_promosi ?></h2>

		<?php if (!empty($halaman_promosi->image_halaman_promosi)): ?>
			<img class="img-responsive img-thumbnail" style="width: 100%" src="<?= base_url('uploads/images/promosis/'.$halaman_promosi->image_halaman_promosi) ?>">

		<?php endif ?>

		<div class="forSMP">
			<div class="isi_halaman_promosi">
				<?= $halaman_promosi->isi_halaman_promosi ?>
				
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<form action="javascript:void(0);" method="POST" id="formPromosi">
					<div class="form-group row">
						<label for="inputEmail3" class="col-sm-3 col-form-label">Nama Lengkap</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="nama_lengkap_siswa_promosi" placeholder="Nama Lengkap">
						</div>
					</div>
					<div class="form-group row">
						<label for="Gelombang" class="col-sm-3 col-form-label">Jenis Kelamin</label>
						<div class="col-sm-9">
							<select name="jenis_kelamin_id" class="form-control">
								<option value=''>--pilih--</option>
								<?php foreach ($jenis_kelamin as $row): ?>
									<option value="<?= $row->id_jenis_kelamin ?>"><?= $row->jenis_kelamin ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Nomor HP</label>
						<div class="col-sm-9">
							<input type="number" class="form-control" name="nomor_hp_siswa_promosi" placeholder="Nomor HP">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Alamat Lengkap</label>
						<div class="col-sm-9">
							<textarea name="alamat_lengkap_siswa_promosi" rows="4" placeholder="Alamat Lengkap" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Email</label>
						<div class="col-sm-9">
							<input type="email" class="form-control" name="email_siswa_promosi" placeholder="Email">
						</div>
					</div>
					<input type="hidden" name="halaman_promosi_id" value="<?= $halaman_promosi->id_halaman_promosi ?>">

					<button type="submit" id="btnOnKirim" class="btn btn-primary">KIRIM</button>
				</div>


			</div>

			<br>


		</div>

	</section>


	<script>
		const URL_FORM_HALAMAN_PROMOSI = `<?= site_url('api/siswa/siswa_promosi') ?>`;

		$('#formPromosi').submit((e) => {
			e.preventDefault();

			axios({
				method: "POST",
				url: `${URL_FORM_HALAMAN_PROMOSI}`,
				data: $('#formPromosi').serialize()
			})
			.then(response => {
				console.log(response);
				if(response.data){
					swal({
						title: "Registrasi Sukses",
						text: `Akan segera diproses. Admin akan menghubungi kamu!`,
						icon: "success",
					})
					.then((success) => {
						if (success) {
							location.href = `<?= site_url()?>`;
						}
					}).then(() => {
						setTimeout(() => {
							location.href = `<?= site_url()?>`;
						}, 2000);
					});
				} 
			})
			.catch(err => {
			// console.log(err);
			let data = err.response.data;
			if(data.status == false){
				swal("Registrasi Gagal", `${data.message}`, "error");
			}
		});

		});
	</script>