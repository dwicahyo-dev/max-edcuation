<style>
.tagname {
    text-align: center;
    text-shadow: 4px 4px 2px rgba(0,0,0,0.6);

}

.section-background {
    background-image: url(<?= base_url('uploads/images/careers/'.$career->career_image) ?>);
    /*min-height: 1000px !important;*/
}

.section-detail {
    max-height: 1000px;
}

.career {
    margin-top: 50px;
    /*margin-top: 100px;*/
}

hr {

    border-width: 5px;
    color: black;
}

.judul-career {
    color: blue;
    font-weight: bold;

}

.job-border {
    border-radius: 10px;
    box-sizing: content-box;
    background-color: white;

    /*color: white;*/
    padding: 20px;
    /*opacity:0.70;*/

    box-shadow: 4px 4px 2px rgba(0,0,0,0.3);
}

.dipasang-pada {
    font-style: italic;
}

.job-border > h5 {
    color: blue;
    font-weight: bold;
}

.positio-detail {
    padding-left: 100;
}

table > .jp {
    color: blue;
}

.job-qualification {
    margin-top: 20px;
}

.btn-apply {
    background-color: green !important;
    border-color: green !important;
    color: white !important;
    border-radius: 50px;
    /*vertical-align: middle;*/
    /*position: fixed;*/

}

.modal-header {
    background-color: blue !important;
    color: white !important;
}

.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  outline: none;
  background: white;
  cursor: inherit;
  display: block;
}

.form-group.has-error label {
  color: #dd4b39;
}

.form-group.has-error .form-control,
.form-group.has-error .input-group-addon {
  border-color: #dd4b39;
  box-shadow: none;
}

.form-group.has-error .help-block {
  color: #dd4b39;
}
.form-group.has-error .help-block {
  color: #dd4b39;
}


</style>

<section class="header11 section-background mbr-fullscreen">
    <div class="container">
        <div class="media-container-column mbr-white col-md-12">
            <h1 class="mbr-section-title py-3 mbr-fonts-style display-3 tagname">
                <strong><?= $career->judul_career ?></strong><br>
            </h1>
        </div>
    </div>

</section>

<section class="header11  features3 cid-qInDyo8xet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="career">
                    <h2 class="mbr-fonts-style judul-career"><?= $career->judul_career ?></h2>
                    <br>
                    <hr noshade>

                    <p class="dipasang-pada">Dipasang pada <?= $career->career_created_at ?></p>
                </div>

                <br>

                <div class="job-border">
                    <h5>JOB POSITION : <?= $career->name_career_job_position ?></h5>

                </div>

                <br>

                <div class="job-border">
                    <h5>JOB AREA : <?= $career->name_career_job_area ?></h5>
                </div>

                <br>

                <div class="job-border">
                    <h5>QUALIFICATION</h5>
                    <img class="img-responsive img-thumbnail" style="width: 100%" src="<?= base_url('uploads/images/careers/'.$career->career_image) ?>">
                    <br>
                    <div class="job-qualification">


                        <?= $career->career_qualification ?>
                    </div>
                </div>

                <br>

                <!-- <a href="#" class="btn btn-apply btn-lg">Apply Job</a> -->

                <button type="button" onclick="addForm()" class="btn btn-apply btn-lg">Apply Job</button>

            </div>
            

        </div>
    </div>

    <div>
        <div class="modal fade" id="modal-form" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header ">
                        <h5>Upload Data Anda</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> &times; 
                            </span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <form id="formCV" action="javascript:void(0)" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="career_id" value="<?= $career->id_career ?>">
                            <div class="form-group">
                                <label class="control-label">Nama Lengkap</label>
                                <div class="form-line">
                                    <input type="text" placeholder="Nama Lengkap" class="form-control" name="name_career_register">
                                    <span class="help-block"></span>
                                </div>
                                
                            </div>

                            <div class="form-group">
                                <label class="control-label">Upload CV (RAR, ZIP, PDF, WORD - FILE MAX : 5MB)</label>
                                <div class="form-line">
                                    <input type="file" class=" form-control" name="cv_career_register">
                                    <span class="help-block"></span>
                                </div>
                                
                            </div>

                            <div class="modal-footer">
                                <button onclick="saveCV()" id="btnSave" class="btn btn-primary btn-save btn-sm">UPLOAD DATA </button>
                                <button type="button" class="btn btn-warning btn-sm " data-dismiss="modal">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </section>

    <script>
        const URL_UPLOAD_DATA_CAREER = `<?= site_url() ?>career/upload_cv`;

        $(document).ready(function() {
           $("input").change(function(){
              $(this).parent().parent().removeClass('has-error');
              $(this).next().empty();
          });

           $('#formCV').submit(function(e) {
              e.preventDefault();


              $('#btnSave').text('Applying...');
              $('#btnSave').attr('disabled',true);

              var formData = new FormData($('#formCV')[0]);
              $.ajax({
                url : `${URL_UPLOAD_DATA_CAREER}`,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data){
                  if(data.status){
                    swal({
                      title: "Applying Success",
                      text: `${data.message}`,
                      icon: "success",
                  }).then((success) => {
                    if (success) {
                        location.href = `<?= site_url()?>`;
                    }
                }).then(() => {
                    $('#formCV')[0].reset();
                })
                .then(() => {
                    setTimeout(() => {
                        location.href = `<?= site_url()?>`;
                    }, 2000);

                });

            } else{
                for (var i = 0; i < data.inputerror.length; i++){
                  $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                  $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
              }
          }
          $('#btnSave').text('UPLOAD DATA');
          $('#btnSave').attr('disabled',false);
      },
      error: function (err){
          swal("Error", "Error Uploading Data", "error");
          $('#btnSave').text('UPLOAD DATA');
          $('#btnSave').attr('disabled',false);
      }
  });
          });


       });

        function addForm() {
            $('#modal-form').modal('show');
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();
            $('#modal-form form')[0].reset();
            

        }
    </script>