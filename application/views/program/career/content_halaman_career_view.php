<style>
.tagname {
    text-align: center;
}

.section-background {
    background-image: url(<?= base_url() ?>assets/images/mbr-1623x1080.jpg);
    min-height: 500px !important;
}

.section-content {
    background-color: #eeeeee;
}

.card-border {
    border-color: black;
}

.section-detail {
    /*padding: 5%;*/
    /*background-color: white !important;*/
}

.card-mantap {
    margin-top: 8%;
}

.btn-load-more {
    background-color: green !important;
    border-color: green !important;
    color: white !important;
    border-radius: 50px;
    /*vertical-align: middle;*/
    /*position: fixed;*/

}

.career_image {
    max-height: 50%;
}

.card-title-text {
    font-weight: bold;
}

</style>

<section class="header11 section-background mbr-fullscreen" id="header11-v">
    <div class="container">
        <div class="media-container-column mbr-white col-md-12">
            <h1 class="mbr-section-title py-3 mbr-fonts-style display-3 tagname">
                <strong>Careers Page</strong><br>
            </h1>

            <p class="mbr-text py-3 mbr-fonts-style display-5 tagname">Bergabunglah Dengan Kami <br> Menjadi Guru Di MAX Education</p>
            
        </div>
    </div>    
</section>

<section class="section-detail">
    <div class="container">
        <div class="row">
            <?php foreach ($careers as $career): ?>
                <div class="col-sm-6">
                    <div class="card card-mantap text-white bg-secondary mb-3">
                        <img class="card-img-top img-fluid" src="<?= base_url('/uploads/images/careers/'.$career->career_image) ?>" alt="Card image cap">
                        <div class="card-body card-border">
                            <h2 class="card-title card-title-text"><?= $career->judul_career ?></h2>
                            <br>
                            <div class="card-text">
                                <?= substr($career->career_qualification, 0, 500)  ?>
                            </div>
                            <a href="<?= site_url('career/'.$career->slug) ?>" class="btn btn-primary btn-sm">Read More</a>
                        </div>
                    </div>

                </div>
            <?php endforeach ?>

        </div>

        <div class="align-center">
            <a href="#" class="btn btn-load-more btn-lg">Load More</a>

        </div>

    </div>


</section>