<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Siswa Inggris
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Siswa Inggris</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <!-- <h3 class="box-title">Hover Data Table</h3> -->

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table id="table_siswa" class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Jenis Kelamin</th>
                    <th>Nomor HP Siswa</th>
                    <th>Nomor HP Orang Tua</th>
                    <th>Alamat Lengkap</th>
                    <th>Domisili</th>
                    <th>Mendaftar Pada</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($siswa_inggris as $row): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $row->nama_lengkap_siswa ?></td>
                    <td><?= $row->jenis_kelamin ?></td>
                    <td><?= $row->nomor_hp_siswa ?></td>
                    <td><?= $row->nomor_hp_orang_tua ?></td>
                    <td><?= $row->alamat_lengkap ?></td>
                    <td><?= $row->domisili ?></td>
                    <td><?= $row->registered ?></td>
                    <td>
                      <a class="btn btn-flat btn-sm btn-primary" href="javascript:void(0)" title="Detail" onclick="detail(<?= $row->id_siswa ?>)"><i class="glyphicon glyphicon-eye-open"></i>   Detail</a>
                    </td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
