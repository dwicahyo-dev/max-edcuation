<style>
.btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  outline: none;
  background: white;
  cursor: inherit;
  display: block;
}

#img-upload{
  width: 100%;
}

</style>

<div class="content-wrapper">
  <section class="content-header">
    <h1>Edit Halaman Career</h1>
    <ol class="breadcrumb">
      <li>
        <a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a>
      </li>
      <li>
        <a href="<?= site_url() ?>admin/career"> Data Halaman Career</a>
      </li>
      <li class="active">Edit Halaman Career</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title ">Edit Halaman Career</h3>
          </div>

          <div class="box-body">
            <form  method="POST" action="javascript:void(0)" id="formEditCareer" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Judul Halaman Career</label>
                    <div class="form-line">
                      <input type="text" name="judul_career" class="form-control" value="<?= isset($career) ? $career->judul_career : '' ?>"  placeholder="Judul Halaman Promosi">
                      <span class="help-block"></span>

                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label">Job Position</label>
                    <div class="form-line">
                      <select name="career_job_position_id" class="form-control" >
                        <option value="">--Pilih--</option>
                        <?php foreach ($career_job_positions as $row): ?>
                          <option <?= $career->id_career_job_position == $row->id_career_job_position ? 'selected' : '' ?> value="<?= $row->id_career_job_position ?>"><?= $row->name_career_job_position ?></option>
                        <?php endforeach ?>
                        
                      </select>
                      <span class="help-block"></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label">Job Area</label>
                    <div class="form-line">
                      <select name="career_job_area_id" class="form-control" >
                        <option value="">--Pilih--</option>
                        <?php foreach ($career_job_areas as $row): ?>
                          <option <?= $career->id_career_job_area == $row->id_career_job_area ? 'selected' : '' ?> value="<?= $row->id_career_job_area ?>"><?= $row->name_career_job_area ?></option>
                        <?php endforeach ?>
                        
                      </select>
                      <span class="help-block"></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label">Qualifications</label>
                    <div class="form-line">
                      <textarea class="textarea" id="text_area_promosi" name="career_qualification" placeholder="Qualifications"
                      style="width: 100%; height: 350px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"  ><?= isset($career) ? $career->career_qualification : '' ?></textarea>
                      <span class="help-block"></span>
                    </div>
                  </div>

                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <?php if (empty($career->career_image)): ?>
                    <img class="img-thumbnail" id='img-upload' src="<?= base_url('uploads/images/promosis/blank.png') ?>" />
                    <?php else: ?>
                      <img class="img-responsive img-thumbnail" id='img-upload' src="<?= base_url('uploads/images/careers/'.$career->career_image) ?>">

                    <?php endif ?>
                  </div>

                  <div class="col-md-4">                  
                    <div class="form-group">
                      <label class="control-label">Upload Image Career</label>
                      <div class="form-line">
                        <input type="file" id="imgInp" name="career_image" class="btn btn-default btn-file" data-toggle="tooltip" data-placement="top" title="Lampirkan File Gambar Career">
                        <span class="help-block"></span>
                      </div>
                    </div>
                    <div class="box-footer">

                      <button id="btnSave" type="submit" class="btn btn-flat btn-primary pull-right">SUBMIT</button>
                      <a href="<?= site_url()?>admin/career" class="btn btn-flat btn-danger ">CANCEL</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  </section>
</div>

<script>
  const URL_UPDATE_CAREER = `<?= site_url('admin/career/update/') ?>`;

  $(document).ready( function() {
    $("textarea").on('change keyup paste', function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });

    $("input").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });

    $("select").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });

    $('#formEditCareer').submit(function(e) {
      e.preventDefault();

      let id_career = `<?= $this->uri->segment(4) ?>`;

      $('#btnSave').text('Menyimpan...');
      $('#btnSave').attr('disabled',true);

      var formData = new FormData($('#formEditCareer')[0]);
      $.ajax({
        url : `${URL_UPDATE_CAREER}${id_career}`,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
          if(data.status){
            swal({
              title: "Sukses",
              text: `${data.message}`,
              icon: "success",
            })
            .then((success) => {
              if (success) {
                location.href = `<?= site_url()?>admin/career`;
              }
            }).then(() => {
              setTimeout(() => {
                location.href = `<?= site_url()?>admin/career`;
              }, 1000);
            });

          } else{
            for (var i = 0; i < data.inputerror.length; i++){
              $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
              $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
            }
          }
          $('#btnSave').text('SUBMIT');
          $('#btnSave').attr('disabled',false);
        },
        error: function (err){
          swal("Error", "Error Adding Data", "error");
          $('#btnSave').text('SUBMIT');
          $('#btnSave').attr('disabled',false);

        }
      });
    });


    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [label]);
    });

  });

  $('.btn-file :file').on('fileselect', function(event, label) {

    var input = $(this).parents('.input-group').find(':text'),
    log = label;

    if( input.length ) {
      input.val(log);
    } else {
      if( log ) alert(log);
    }

  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#img-upload').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function(){
    readURL(this);
  });


</script>
