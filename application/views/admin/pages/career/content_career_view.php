<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Halaman Career
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Halaman Career</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="<?= site_url()?>admin/career/add" role="button" class="btn btn-flat btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Data Halaman Career</a>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th style="width: 5%">No</th>
                    <th style="width: 15%">Image</th>
                    <th >Judul Halaman Career</th>
                    <th >Job Position</th>
                    <th >Job Area</th>
                    <th >Kualifikasi</th>
                    <th>Link Career</th>
                    <th >Uploaded at</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($careers as $row): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td>
                      <?php if (empty($row->career_image)): ?>
                        <p>Tidak ada gambar</p>
                        <?php else: ?>
                          <img class="img-responsive img-thumbnail" id='img-upload' style="width: 100%" src="<?= base_url('uploads/images/careers/'.$row->career_image) ?>">

                        <?php endif ?>
                      </td>
                      <td><?= $row->judul_career ?></td>
                      <td><?= $row->name_career_job_position ?></td>
                      <td><?= $row->name_career_job_area ?></td>
                      <td><?= substr($row->career_qualification, 0, 500) ?></td>
                      <td>
                        <a href="<?= site_url('career/'). $row->slug ?>" target="_blank"><?= site_url('career/'). $row->slug ?></a>

                      </td>
                      <td><?= $row->career_created_at ?></td>
                      <td>
                        <!-- <a href="javascript:void(0);" onclick="detailCareer(<?= $row->id_career ?>)" class="btn btn-flat btn-primary" data-toggle="tooltip" data-placement="bottom" title="Detail"><i class="fa fa-eye"></i></a> -->
                        <a href="<?= site_url('admin/career/edit/'.$row->id_career) ?>" class="btn btn-flat btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0);" onclick="deleteCareer(<?= $row->id_career ?>)" class="btn btn-flat btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>

                      </td>
                    </tr>
                  <?php endforeach ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
</div>


<script>

  const URL_DELETE_CAREER = `<?= site_url()?>admin/career/delete/`;
    // const URL_DETAIL_CAREER = `<?= site_url()?>admin/career/view/`;

    $(document).ready(function() {
      $('table').DataTable();

    });

    function deleteCareer(id_career) {
      swal({
        title: "Yakin Ingin Menghapus ?",
        text: "Data Yang Anda Pilih Akan Dihapus Dari Database",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            type: "POST",
            url: `${URL_DELETE_CAREER}${id_career}`,
            dataType: 'JSON',
            success: function(response){
              console.log(response);
              swal({
                title: "Sukses",
                text: `${response.message}`,
                icon: "success",
              })
              .then((success) => {
                if (success) {
                  location.reload();
                }
              })
              .then(() => {
                setTimeout(() => {
                  location.reload();
                }, 1000);
              });
            },
            error : function(err){
            // console.log(err);
            swal("Gagal", "Data Gagal Dihapus", "error");
          }
        });
        }
      });
    }

  </script>
