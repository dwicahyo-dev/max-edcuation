<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Halaman Career - Register
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Halaman Career - Register</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th style="width: 5%">No</th>
                    <th style="width: 25%">Nama Lengkap Pelamar</th>
                    <th style="width: 20%">Melamar Pada Career</th>
                    <th style="width: 20%">CV</th>
                    <th style="width: 20%">Melamar Pada</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($career_registers as $row): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $row->name_career_register ?></td>
                    <td><?= $row->judul_career ?></td>
                    <td>
                      <a href="<?= site_url('admin/careerregister/cv/download/'. $row->id_career_register); ?>"><?= $row->cv_career_register?></a>
                    </td>
                    <td><?= $row->career_register_created_at ?></td>
                    <td>
                      <!-- <a href="<?= site_url('admin/career/edit/'.$row->id_career_register) ?>" class="btn btn-flat btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> -->
                      <a href="javascript:void(0);" onclick="deleteCareer(<?= $row->id_career_register ?>)" class="btn btn-flat btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>

                    </td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
</div>


<script>

  const URL_DELETE_CAREER = `<?= site_url()?>admin/careerregister/delete/`;

  $(document).ready(function() {
    $('table').DataTable();

  });

  function deleteCareer(id_career_register) {      
    swal({
      title: "Yakin Ingin Menghapus ?",
      text: "Data Yang Anda Pilih Akan Dihapus Dari Database",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          type: "POST",
          url: `${URL_DELETE_CAREER}${id_career_register}`,
          dataType: 'JSON',
          success: function(response){
            console.log(response);
            swal({
              title: "Sukses",
              text: `${response.message}`,
              icon: "success",
            })
            .then((success) => {
              if (success) {
                location.reload();
              }
            })
            .then(() => {
              setTimeout(() => {
                location.reload();
              }, 1000);
            });
          },
          error : function(err){
            // console.log(err);
            swal("Gagal", "Data Gagal Dihapus", "error");
          }
        });
      }
    });
  }

</script>
