<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Halaman Promosi
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Halaman Promosi</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="<?= site_url()?>admin/halaman_promosi/add" role="button" class="btn btn-flat btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Data Halaman Promosi</a>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th style="width: 5%">No</th>
                    <th style="width: 15%">Image</th>
                    <th >Nama Halaman Promosi</th>
                    <th >Isi Halaman Promosi</th>
                    <th>Link Promosi</th>
                    <th >Uploaded at</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($promosis as $row): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td>
                      <?php if (empty($row->image_halaman_promosi)): ?>
                        <p>Tidak ada gambar</p>
                        <?php else: ?>
                          <img class="img-responsive img-thumbnail" id='img-upload' style="width: 100%" src="<?= base_url('uploads/images/promosis/'.$row->image_halaman_promosi) ?>">

                        <?php endif ?>
                      </td>
                      <td><?= $row->nama_halaman_promosi ?></td>
                      <td><?= substr($row->isi_halaman_promosi, 0, 500) ?></td>
                      <td>
                        <a href="<?= site_url(). $row->slug ?>" target="_blank"><?= site_url(). $row->slug ?></a>
  
                        </td>
                      <td><?= $row->created_at ?></td>
                      <td>
                        <a href="<?= site_url('admin/halaman_promosi/edit/'.$row->id_halaman_promosi) ?>" class="btn btn-flat btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0);" onclick="deletePromosi(<?= $row->id_halaman_promosi ?>)" class="btn btn-flat btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  <?php endforeach ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
</div>

<script>

  const URL_DELETE_PROMOSI = `<?= site_url()?>admin/halaman_promosi/delete/`;

  $(document).ready(function() {
    $('table').DataTable();

  });

  function deletePromosi(id_halaman_promosi) {
    swal({
      title: "Yakin Ingin Menghapus ?",
      text: "Data Yang Anda Pilih Akan Dihapus Dari Database",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          type: "POST",
          url: `${URL_DELETE_PROMOSI}${id_halaman_promosi}`,
          dataType: 'JSON',
          success: function(response){
            // console.log(response);
            swal({
              title: "Sukses",
              text: `${response.message}`,
              icon: "success",
            })
            .then((success) => {
              if (success) {
                location.reload();
              }
            })
            .then(() => {
              setTimeout(() => {
                location.reload();
              }, 1000);
            });
          },
          error : function(err){
            // console.log(err);
            swal("Gagal", "Data Gagal Dihapus", "error");
          }
        });
      }
    });
  }

</script>
