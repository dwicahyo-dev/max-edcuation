<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Siswa Halaman Promosi
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Siswa Halaman Siswa Promosi</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">

          </div>
          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th style="width: 5%">No</th>
                    <th style="width: 20%">Nama Lengkap Siswa</th>
                    <th style="width: 15%">Jenis Kelamin</th>
                    <th style="width: 15%">Nomor HP Siswa</th>
                    <th style="width: 15%">Email</th>
                    <th style="width: 15%">Alamat</th>
                    <th style="width: 15%">Mendaftar Pada Halaman Promosi</th>
                    <th style="width: 13%">Mendaftar Pada</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($siswa_promosis as $row): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $row->nama_lengkap_siswa_promosi ?></td>
                    <td><?= $row->jenis_kelamin ?></td>
                    <td><?= $row->nomor_hp_siswa_promosi ?></td>
                    <td><?= $row->email_siswa_promosi ?></td>
                    <td><?= $row->alamat_lengkap_siswa_promosi ?></td>
                    <td><?= $row->nama_halaman_promosi ?></td>
                    <td><?= $row->created_at ?></td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
</div>

<script>
  $(document).ready(function() {
    $('table').DataTable();

  });

</script>
