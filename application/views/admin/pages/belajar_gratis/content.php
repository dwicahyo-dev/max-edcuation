<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Siswa Belajar Gratis
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Siswa Belajar Gratis</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table id="tableBelajarGratis" class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Gelombang</th>
                    <th>Nama IG</th>
                    <th>Mendaftar Pada</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($belajar_gratis as $siswa_belajar_gratis): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $siswa_belajar_gratis->nama_lengkap_siswa ?></td>
                    <td><?= $siswa_belajar_gratis->gelombang ?></td>
                    <td><?= $siswa_belajar_gratis->nama_ig ?></td>
                    <td><?= $siswa_belajar_gratis->registered ?></td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>

<script>
  $(document).ready(function() {
    $('#tableBelajarGratis').DataTable();

  });

</script>