<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Paket Program
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Paket Program</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <button type="button" onclick="addForm()" class="btn btn-flat btn-primary pull-right">
              <i class="fa fa-plus"></i> Tambah Data Paket Program
            </button>
          </div>

          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table id="table_guru" class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th style="width: 5%">No</th>
                    <th style="width: 20%">Nama Program</th>
                    <th style="width: 25%">Nama Paket</th>
                    <th style="width: 18%">Harga Paket</th>
                    <th style="width: 10%">Waktu Belajar</th>
                    <th style="width: 10%">Jumlah Pertemuan</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($paket_programs as $paket_program): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $paket_program->program ?></td>
                    <td><?= $paket_program->nama_paket ?></td>
                    <td>Rp. <?= number_format($paket_program->harga_paket,2,",",".") ?></td>
                    <td><?= $paket_program->waktu_belajar ?> jam</td>
                    <td><?= $paket_program->jumlah_pertemuan ?>x</td>
                    <td>
                      <a href="javascript:void(0);" onclick="editPaketProgram(<?= $paket_program->id_paket_program ?>)" class="btn btn-flat btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
                        <i class="fa fa-pencil"></i>
                      </a> 
                      <a href="javascript:void(0);" onclick="deletePaketProgram(<?= $paket_program->id_paket_program ?>)" class="btn btn-flat btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>

                <?php endforeach ?>
              </tbody>

            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
</div>

<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form class="form-horizontal" id="formPaketProgram" action="javascript:void(0)" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
          <h3 class="modal-title"></h3>
        </div>

        <div class="modal-body">
          <input type="hidden" id="id" value="">

          <div class="form-group">
            <label for="kategori" class="col-md-3 control-label">Nama Program</label>
            <div class="col-md-6">
              <div class="form-line">
                <select class="form-control" name="program_id" id="id_program">
                  <option value=''>--pilih--</option>
                  <?php foreach ($programs as $program): ?>
                    <option value="<?= $program->id_program ?>"><?= $program->program ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="kategori" class="col-md-3 control-label">Nama Paket</label>
            <div class="col-md-6">
              <div class="form-line">
                <select class="form-control"  name="paket_id" id="id_paket">
                  <option value=''>--pilih--</option>
                  <?php foreach ($pakets as $paket): ?>
                    <option value="<?= $paket->id_paket ?>"><?= $paket->nama_paket ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="kategori" class="col-md-3 control-label">Harga Paket</label>
            <div class="col-md-6">
              <div class="form-line">
                <input type="number" class="form-control" name="harga_paket" id="harga_paket"  placeholder="Harga Paket" >
                <span class="help-block"></span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="kategori" class="col-md-3 control-label">Waktu Belajar</label>
            <div class="col-md-6">
              <div class="form-line">
                <input type="number" class="form-control" name="waktu_belajar" id="waktu_belajar" placeholder="Waktu Belajar (dalam jam)">
                <span class="help-block"></span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="kategori" class="col-md-3 control-label">Jumlah Pertemuan</label>
            <div class="col-md-6">
              <div class="form-line">
                <input type="number" class="form-control" name="jumlah_pertemuan" id="jumlah_pertemuan" placeholder="Jumlah Pertemuan">
                <span class="help-block"></span>
              </div>
            </div>
          </div>

        </div>


        <div class="modal-footer">
          <button onclick="savePaketProgram()" id="btnSave" class="btn btn-flat btn-primary btn-save"> Simpan </button>
          <button type="button" class="btn btn-flat btn-warning" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  const URL_TAMBAH_PAKET_PROGRAM  = `<?= site_url('admin/paket_program/add')?>`;
  const URL_EDIT_PAKET_PROGRAM    = `<?= site_url('admin/paket_program/edit/')?>`;
  const URL_DELETE_PAKET_PROGRAM  = `<?= site_url('admin/paket_program/delete/')?>`;
  const URL_UPDATE_PAKET_PROGRAM  = `<?= site_url('admin/paket_program/update/')?>`;

  let save_method;
  let url_method;

  $(document).ready(function() {
    $("input").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });

    $("select").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });

    $('table').DataTable();

  });

  function addForm() {
    save_method = 'add';

    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();
    $('.modal-title').text('Tambah Data Paket Pada Setiap Program');
  }

  function savePaketProgram(){
    $('#btnSave').text('Menyimpan...'); 
    $('#btnSave').attr('disabled',true);

    // let program_id = $("input[name='program_id']").val();
    // let paket_id = $("input[name='paket_id']").val();
    // let harga_paket = $("input[name='harga_paket']").val();
    // let waktu_belajar = $("input[name='waktu_belajar']").val();
    // let jumlah_pertemuan = $("input[name='jumlah_pertemuan']").val();

    let id_paket_program = $('#id').val();

    let formData = new FormData($('#formPaketProgram')[0]);

    if(save_method == 'add') {
      url_method = `${URL_TAMBAH_PAKET_PROGRAM}`;
    } else {
      url_method = `${URL_UPDATE_PAKET_PROGRAM}${id_paket_program}`;
    }

    $.ajax({
      url : url_method,
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      dataType: "JSON",
      success: function(data){
        console.log(data);
        if(data.success){
          swal({
            title: "Sukses",
            text: `${data.message}`,
            icon: "success",
          })
          .then((success) => {
            if (success) {
              location.reload();
            }
          })
          .then(() => {
            $('#modal-form').modal('hide');
          })
          .then(() => {
            setTimeout(() => {
              location.reload();
            }, 1000);
          });

        }
        else{
          for (var i = 0; i < data.inputerror.length; i++){
            $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
            $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
          }
        }
        $('#btnSave').text('Simpan');
        $('#btnSave').attr('disabled',false);
      },
      error: function(err){
        swal("Gagal", "Data Gagal Disimpan", "error");
      }
    });
  }

  function editPaketProgram(id){

    save_method = 'update';

    $.ajax({
      url : `${URL_EDIT_PAKET_PROGRAM}${id}`,
      type : "GET",
      dataType : "JSON",
      success : function(response){
        let data = response.data;
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Data Paket Pada Setiap Program');
        $('#id').val(data.id_paket_program);
        $('#id_program').val(data.program_id);
        $('#id_paket').val(data.paket_id);
        $('#harga_paket').val(data.harga_paket);
        $('#waktu_belajar').val(data.waktu_belajar);
        $('#jumlah_pertemuan').val(data.jumlah_pertemuan);

        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

      },
      error : function(){
        swal("Gagal", "Data Gagal Ditampilkan", "error");
      }
    });
  }

  function deletePaket(id){
    swal({
      title: "Yakin Ingin Menghapus ?",
      text: "Data Yang Anda Pilih Akan Dihapus Dari Database",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          type: "POST",
          url: `${URL_DELETE_PAKET}${id}`,
          dataType: 'JSON',
          success: function(response){
            // console.log(response);
            swal({
              title: "Sukses",
              text: `${response.message}`,
              icon: "success",
            })
            .then((success) => {
              if (success) {
                location.reload();
              }
            }).then(() => {
              setTimeout(() => {
                location.reload();
              }, 1000);
            });
          },
          error : function(){
            swal("Gagal", "Data Gagal Dihapus", "error");
          }
        });
      }
    });
  }

  function deletePaketProgram(id_paket_program){    
    swal({
      title: "Yakin Ingin Menghapus ?",
      text: "Data Yang Anda Pilih Akan Dihapus Dari Database",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url : `${URL_DELETE_PAKET_PROGRAM}${id_paket_program}`,
          type: "POST",
          dataType: "JSON",
          success: function(data){
            // console.log(data);
            if(data.success){
              swal({
                title: "Sukses",
                text: `${data.message}`,
                icon: "success",
              })
              .then((success) => {
                if (success) {
                  location.reload();
                }
              })
              .then(() => {
                $('#modal-form').modal('hide');
              })
              .then(() => {
                setTimeout(() => {
                  location.reload();
                }, 2000);
              });

            }
          },
          error: function (err){
            swal("Error", "Delete Data Gagal", "error");
          }
        });

      }
    });

  }
</script>
