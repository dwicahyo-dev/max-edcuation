<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Paket
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= site_url() ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Paket</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <button type="button" onclick="addForm()" class="btn btn-flat btn-primary pull-right">
              <i class="fa fa-plus"></i> Tambah Data Paket
            </button>
          </div>

          <div class="box-body">
            <div class="col-md-12 table-responsive">
              <table id="table_guru" class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th style="width: 5%">No</th>
                    <th style="width: 75%">Nama Paket</th>
                    <th class="actions">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; foreach ($pakets as $paket): ?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $paket->nama_paket ?></td>
                    <td>
                      <a href="javascript:void(0);" onclick="editPaket(<?= $paket->id_paket ?>)" class="btn btn-flat btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit">
                        <i class="fa fa-pencil"></i>
                      </a> 
                      <a href="javascript:void(0);" onclick="deletePaket(<?= $paket->id_paket ?>)" class="btn btn-flat btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>

                <?php endforeach ?>
              </tbody>

            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
</div>

<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form class="form-horizontal" id="formPaket" action="javascript:void(0)" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
          <h3 class="modal-title"></h3>
        </div>

        <div class="modal-body">
          <div class="form-group">
            <label class="col-md-3 control-label">Nama Paket</label>
            <div class="col-md-6">
              <div class="form-line">
                <input type="hidden" id="id" value="">
                <input id="nama_paket" name="nama_paket" placeholder="Nama Paket" type="text" class="form-control" autofocus="true">
                <span class="help-block"></span>
              </div>
            </div>
          </div>
        </div>


        <div class="modal-footer">
          <button onclick="savePaket()" id="btnSave" class="btn btn-flat btn-primary btn-save"> Simpan </button>
          <button type="button" class="btn btn-flat btn-warning" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  const URL_TAMBAH_PAKET  = `<?= site_url('admin/paket/add')?>`;
  const URL_EDIT_PAKET    = `<?= site_url('admin/paket/edit/')?>`;
  const URL_DELETE_PAKET  = `<?= site_url('admin/paket/delete/')?>`;
  const URL_UPDATE_PAKET  = `<?= site_url('admin/paket/update/')?>`;

  let save_method;
  let url_method;

  $(document).ready(function() {
    $("input").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });

    $('table').DataTable();

  });

  function addForm() {
    save_method = 'add';

    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();
    $('.modal-title').text('Tambah Data Paket');
  }

  function savePaket(){
    $('#btnSave').text('Menyimpan...'); 
    $('#btnSave').attr('disabled',true);

    let nama_paket = $("input[name='nama_paket']").val();

    let id_paket = $('#id').val();

    if(save_method == 'add') {
      url_method = `${URL_TAMBAH_PAKET}`;
    } else {
      url_method = `${URL_UPDATE_PAKET}${id_paket}`;
    }

    $.ajax({
      type: "POST",
      url: url_method,
      dataType: 'JSON',
      data: {
        nama_paket: nama_paket
      },
      success: function(data){
        console.log(data);
        if(data.success){
          swal({
            title: "Sukses",
            text: `${data.message}`,
            icon: "success",
          })
          .then((success) => {
            if (success) {
              location.reload();
            }
          })
          .then(() => {
            $('#modal-form').modal('hide');
          })
          .then(() => {
            setTimeout(() => {
              location.reload();
            }, 1000);
          });

        }
        else{
          for (var i = 0; i < data.inputerror.length; i++){
            $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
            $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
          }
        }
        $('#btnSave').text('Simpan');
        $('#btnSave').attr('disabled',false);
      },
      error: function(err){
        swal("Gagal", "Data Gagal Disimpan", "error");
      }
    });
  }

  function editPaket(id){

    save_method = 'update';

    $.ajax({
      url : `${URL_EDIT_PAKET}${id}`,
      type : "GET",
      dataType : "JSON",
      success : function(response){
        let data = response.data;
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Data Paket');
        $('#id').val(data.id_paket);
        $('#nama_paket').val(data.nama_paket);

        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

      },
      error : function(){
        swal("Gagal", "Data Gagal Ditampilkan", "error");
      }
    });
  }

  function deletePaket(id){
    swal({
      title: "Yakin Ingin Menghapus ?",
      text: "Data Yang Anda Pilih Akan Dihapus Dari Database",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          type: "POST",
          url: `${URL_DELETE_PAKET}${id}`,
          dataType: 'JSON',
          success: function(response){
            // console.log(response);
            swal({
              title: "Sukses",
              text: `${response.message}`,
              icon: "success",
            })
            .then((success) => {
              if (success) {
                location.reload();
              }
            }).then(() => {
              setTimeout(() => {
                location.reload();
              }, 1000);
            });
          },
          error : function(){
            swal("Gagal", "Data Gagal Dihapus", "error");
          }
        });
      }
    });
  }
</script>
