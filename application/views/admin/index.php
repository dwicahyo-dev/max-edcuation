<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title?></title>
  <link rel="shortcut icon" href="<?= base_url() ?>assets/images/logo-max-education21-1041x635.png" type="image/x-icon">

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">

  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/select2/dist/css/select2.min.css">

  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap4.min.css">
  <!-- <link rel="stylesheet" href="<?= base_url() ?>assets/sweetalert/sweetalert.css"> -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/sweetalert/sweetalert.css">

  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/dist/css/skins/_all-skins.min.css">
  <!-- <link type="text/css" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet"> -->

  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  


  <!-- jQuery 3 -->
  <!-- <script src="<?= base_url() ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script> -->
  <script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
  
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= base_url() ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <script src="<?= base_url() ?>assets/sweetalert/sweetalert.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <!-- <link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/select2/dist/js/select2.min.js"> -->

  <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script> -->

  <!-- DataTables -->
  <script src="<?= base_url() ?>assets/admin/bower_components/datatables.net-bs/js/jquery.dataTables.min.js"></script>

  <!-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->

  <script src="<?= base_url() ?>assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap4.min.js"></script>

  <!-- bootstrap datepicker -->
  <script src="<?= base_url() ?>assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

  <script src="<?= base_url() ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=licx92izrojs3qg0fw9qm82miy1qqdeywkoc6irzjjxvct8u"></script>

  <script>tinymce.init({
    selector:'#text_area_promosi',
    // height: 500,
    block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
    // formats: {
    //   alignleft: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
    //   aligncenter: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
    //   alignright: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
    //   alignjustify: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
    //   bold: {inline : 'span', 'classes' : 'bold'},
    //   italic: {inline : 'span', 'classes' : 'italic'},
    //   underline: {inline : 'span', 'classes' : 'underline', exact : true},
    //   strikethrough: {inline : 'del'},
    //   forecolor: {inline : 'span', classes : 'forecolor', styles : {color : '%value'}},
    //   hilitecolor: {inline : 'span', classes : 'hilitecolor', styles : {backgroundColor : '%value'}},
    //   custom_format: {block : 'h1', attributes : {title : 'Header'}, styles : {color : 'red'}}
    // },
    plugins: 'code advcode table wordcount textcolor',
    toolbar: 'undo redo | fontsizeselect fontselect | code | alignleft aligncenter alignright alignjustify | bold italic underline strikethrough | indent outdent | forecolor backcolor',
    content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css',
    '//fonts.googleapis.com/css?family=Indie+Flower','//fonts.googleapis.com/css?family=Lato:300,300i,400,400i', '//www.tinymce.com/css/codepen.min.css'],
    font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
  });
</script>

<!-- <script>
  tinymce.init({
    selector: 'textarea',
    // height: 500,
    block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
    plugins: 'table wordcount code advcode table',
    content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css',
    '//fonts.googleapis.com/css?family=Indie+Flower','//fonts.googleapis.com/css?family=Lato:300,300i,400,400i', '//www.tinymce.com/css/codepen.min.css'],

    style_formats: [
    { title: 'Bold text', inline: 'strong' },
    { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
    { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
    { title: 'Badge', inline: 'span', styles: { display: 'inline-block', border: '1px solid #2276d2', 'border-radius': '5px', padding: '2px 5px', margin: '0 2px', color: '#2276d2' } },
    { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
    ],
    formats: {
      alignleft: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'left' },
      aligncenter: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'center' },
      alignright: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'right' },
      alignfull: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'full' },
      bold: { inline: 'span', 'classes': 'bold' },
      italic: { inline: 'span', 'classes': 'italic' },
      underline: { inline: 'span', 'classes': 'underline', exact: true },
      strikethrough: { inline: 'del' },
      customformat: { inline: 'span', styles: { color: '#00ff00', fontSize: '20px' }, attributes: { title: 'My custom format' }, classes: 'example1' },
    },
    toolbar: 'undo redo |  fontsizeselect fontselect | code | alignleft aligncenter alignright alignjustify blockquote strikethrough',
    font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',

  });
tinymce.activeEditor.formatter.apply('aligncenter');

</script> -->



<!-- <script>
  tinymce.init({ selector:'textarea',

});</script> -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114932927-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114932927-1');
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-7752890002100313",
    enable_page_level_ads: true
  });
</script>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed">
  <div class="wrapper">

    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="<?= site_url() ?>admin" class="navbar-brand"><b>Maximize Yourself</b></a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <form class="navbar-form navbar-left" role="search">
              <div class="form-group">
                <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
              </div>
            </form>
          </div>
          <!-- /.navbar-collapse -->
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="<?= base_url('uploads/images/avatars/'.$user_admin->avatar) ?>" class="user-image" >
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?= $user_admin->nama_lengkap_admin ?> </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="<?= base_url('uploads/images/avatars/'.$user_admin->avatar) ?>" class="img-circle" >

                    <p>
                      <?= $user_admin->nama_lengkap_admin ?> - <?= $user->username ?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?= site_url()?>admin/profile" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat" id="btnLogout">Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
      </nav>
    </header>
    <!-- Full Width Column -->

    <?= $content ?>

    <div class="modal fade" id="modal_detail_siswa" role="document">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Detail Siswa</h4>
            </div>
            <div class="modal-body">
              <div id="detail_siswa">
                <form>
                  <fieldset disabled>
                    <div class="form-group">
                      <label>Nama Lengkap</label>
                      <input type="text" class="form-control" id="nama_lengkap_siswa">
                    </div>
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <input type="text" class="form-control" id="jenis_kelamin_siswa">
                    </div>
                    <div class="form-group">
                      <label>Nomor HP Siswa</label>
                      <input type="text" class="form-control" id="nomor_hp_siswa">
                    </div>
                    <div class="form-group">
                      <label>Nomor HP Orang Tua</label>
                      <input type="text" class="form-control" id="nomor_hp_orang_tua">
                    </div>
                    <div class="form-group">
                      <label>Alamat Lengkap</label>
                      <textarea class="form-control" rows="4" id="alamat_lengkap_siswa"></textarea>
                    </div>
                    <div class="form-group">
                      <label>Domisili</label>
                      <input type="text" class="form-control" id="domisili_siswa">
                    </div>
                    <div class="form-group">
                      <label>Tingkat Sekolah</label>
                      <input type="text" class="form-control" id="tingkat_sekolah_siswa">
                    </div>
                    <div class="form-group">
                      <label>Program</label>
                      <input type="text" class="form-control" id="program_siswa">
                    </div>
                    <div class="form-group">
                      <label>Kelas</label>
                      <input type="text" class="form-control" id="kelas_siswa">
                    </div>
                    <div class="form-group">
                      <label>Mata Pelajaran</label>
                      <div id="mata_pelajaran_siswa">

                      </div>
                    </div>
                    <div class="form-group">
                      <label>Hari</label>
                      <div id="hari_siswa">

                      </div>
                    </div>
                    <div class="form-group">
                      <label>Jam</label>
                      <div id="jam_siswa">

                      </div>
                    </div>
                    <div class="form-group">
                      <label>Mendaftar Pada</label>
                      <input type="text" class="form-control" id="mendaftar_pada_siswa">
                    </div>
                    
                  </fieldset>
                </form>

              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="container">
          <div class="pull-right hidden-xs">
            <b>Version</b> 1.3.0
          </div>
          <strong>Copyright &copy; 2018 <a href="http://www.maxeducation.id/" target="_blank">MAX Education</a>.</strong> All rights
          reserved.
        </div>
        <!-- /.container -->
      </footer>
    </div>
    <!-- ./wrapper -->


    <!-- SlimScroll -->
    <script src="<?= base_url() ?>assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url() ?>assets/admin/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url() ?>assets/admin/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url() ?>assets/admin/dist/js/demo.js"></script>

    <script>
      const URL_LOGOUT = `<?= site_url('logout') ?>`;

      $(document).ready(() => {
        $('#table_siswa').DataTable({
          "oLanguage": {
            "sProcessing": "Loading...",
            "sSearch": "Search Data :  ",
            "sZeroRecords": "Data Kosong",
            "sEmptyTable": "No data available in table"
          },
          "iDisplayLength": 50,
          "order": [[ 7, "desc" ]],
        });

        $('#btnLogout').click(() => {
          swal({
            title: "Yakin Ingin Keluar ?",
            text: "Apakah Kamu Yakin Ingin Keluar Dari Halaman Ini ??",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(willLogout => {
            if (willLogout) {
              location.href = `${URL_LOGOUT}`;
            }
          });

        });
      });

      function detail(id){
        $("#mata_pelajaran_siswa").empty();
        $("#hari_siswa").empty();
        $("#jam_siswa").empty();

        $.ajax({
          url : `<?= site_url('detail_siswa/')?>${id}`,
          type: "GET",
          success: function(data){
            // console.log(data);
            $('#modal_detail_siswa').modal('show');
            $('#nama_lengkap_siswa').val(data.siswa.nama_lengkap_siswa);
            $('#jenis_kelamin_siswa').val(data.siswa.jenis_kelamin);
            $('#nomor_hp_siswa').val(data.siswa.nomor_hp_siswa);
            $('#nomor_hp_orang_tua').val(data.siswa.nomor_hp_orang_tua);
            $('#alamat_lengkap_siswa').val(data.siswa.alamat_lengkap);
            $('#domisili_siswa').val(data.siswa.domisili);
            $('#tingkat_sekolah_siswa').val(data.siswa.tingkat_sekolah);
            $('#program_siswa').val(data.siswa.program);
            $('#kelas_siswa').val(data.siswa.kelas);
            $('#mendaftar_pada_siswa').val(data.siswa.registered);

            for (var i = 0; i < data.mata_pelajaran.length; i++) {
              $('#mata_pelajaran_siswa').append(`
                <input type="text" class="form-control" value="${data.mata_pelajaran[i].mata_pelajaran}">
                <br>
                `);
            }

            for (var i = 0; i < data.hari.length; i++) {
              $('#hari_siswa').append(`
                <input type="text" class="form-control" value="${data.hari[i].hari}">
                <br>

                `);
            }

            for (var i = 0; i < data.jam.length; i++) {
              $('#jam_siswa').append(`
                <input type="text" class="form-control" value="${data.jam[i].jam}">
                <br>

                `);
            }
          }, error: function (jqXHR, textStatus, errorThrown){
            alert('Error getting data siswa');
          }
        });
      }

    </script>

  </body>
  </html>
