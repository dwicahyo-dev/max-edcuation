-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 03, 2018 at 04:18 PM
-- Server version: 10.1.36-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nirwannd_max`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_lengkap_admin` varchar(255) NOT NULL,
  `jenis_kelamin_id` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_lengkap_admin`, `jenis_kelamin_id`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Admin Max', 1, '1522818496967.png', '2018-03-24 10:21:14', '2018-06-02 06:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE `agama` (
  `id_agama` int(11) NOT NULL,
  `agama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`id_agama`, `agama`) VALUES
(1, 'Islam'),
(2, 'Kristen Protestan'),
(3, 'Khatolik'),
(4, 'Hindu'),
(5, 'Budha'),
(6, 'Khonghucu'),
(7, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id_career` int(11) NOT NULL,
  `judul_career` varchar(255) NOT NULL,
  `career_job_position_id` int(11) NOT NULL,
  `career_job_area_id` int(11) NOT NULL,
  `career_image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `career_qualification` text NOT NULL,
  `career_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `career_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id_career`, `judul_career`, `career_job_position_id`, `career_job_area_id`, `career_image`, `slug`, `career_qualification`, `career_created_at`, `career_updated_at`) VALUES
(3, 'Guru Fisika SMA Bekasi', 2, 2, '1539760076097.jpeg', 'guru-fisika-sma-bekasi', '<p>1. Berasal dari jurusan Fisika atau yang masih berhubungan</p>\r\n<p>2. Mampu mengerjakan dan mengajar pelajaran Fisika tingkat SMA</p>\r\n<p>3. Bersedia bekerja Part Time di area Bekasi</p>\r\n<p>4. Suka mengajar</p>\r\n<p>5. Sabar</p>', '2018-10-17 07:07:56', '2018-10-17 07:25:29'),
(4, 'Guru Fisika SMA Jakarta', 2, 1, '1539760987542.jpeg', 'guru-fisika-sma-jakarta', '<p>1. Berasal dari jurusan Fisika atau yang masih berhubungan</p>\r\n<p>2. Mampu mengerjakan dan mengajar pelajaran Fisika tingkat SMA</p>\r\n<p>3. Bersedia bekerja Part Time di area Bekasi</p>\r\n<p>4. Suka mengajar</p>\r\n<p>5. Sabar</p>', '2018-10-17 07:23:07', '2018-10-17 07:25:17'),
(5, 'Guru Matematika SMA Jakarta', 3, 1, '1539761248654.jpeg', 'guru-matematika-sma-jakarta', '<p>1. Berasal dari jurusan Matematika atau yang masih berhubungan</p>\r\n<p>2. Mampu mengerjakan dan mengajar pelajaran Matematika tingkat SMA</p>\r\n<p>3. Bersedia bekerja Part Time di Area Jakarta</p>\r\n<p>4. Suka mengajar</p>\r\n<p>5. Sabar</p>', '2018-10-17 07:27:28', '2018-10-17 07:27:28'),
(6, 'Guru Matematika SMA Bekasi', 3, 2, '1539761382934.jpeg', 'guru-matematika-sma-bekasi', '<p style=\"color: #626262;\">1. Berasal dari jurusan Matematika atau yang masih berhubungan</p>\r\n<p style=\"color: #626262;\">2. Mampu mengerjakan dan mengajar pelajaran Matematika tingkat SMA</p>\r\n<p style=\"color: #626262;\">3. Bersedia bekerja Part Time di Area Jakarta</p>\r\n<p style=\"color: #626262;\">4. Suka mengajar</p>\r\n<p style=\"color: #626262;\">5. Sabar</p>', '2018-10-17 07:29:42', '2018-10-17 07:29:42');

-- --------------------------------------------------------

--
-- Table structure for table `career_job_area`
--

CREATE TABLE `career_job_area` (
  `id_career_job_area` int(11) NOT NULL,
  `name_career_job_area` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career_job_area`
--

INSERT INTO `career_job_area` (`id_career_job_area`, `name_career_job_area`) VALUES
(1, 'Jakarta'),
(2, 'Bekasi'),
(3, 'Karawang'),
(4, 'Depok');

-- --------------------------------------------------------

--
-- Table structure for table `career_job_position`
--

CREATE TABLE `career_job_position` (
  `id_career_job_position` int(11) NOT NULL,
  `name_career_job_position` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career_job_position`
--

INSERT INTO `career_job_position` (`id_career_job_position`, `name_career_job_position`) VALUES
(1, 'Guru Matematika SD TEST'),
(2, 'Guru Fisika'),
(3, 'Guru Matematika'),
(4, 'Guru Bahasa Inggris'),
(5, 'Guru Kimia'),
(7, 'Guru Ekonomi');

-- --------------------------------------------------------

--
-- Table structure for table `career_register`
--

CREATE TABLE `career_register` (
  `id_career_register` int(11) NOT NULL,
  `name_career_register` varchar(255) NOT NULL,
  `cv_career_register` varchar(255) NOT NULL,
  `career_id` int(11) NOT NULL,
  `career_register_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career_register`
--

INSERT INTO `career_register` (`id_career_register`, `name_career_register`, `cv_career_register`, `career_id`, `career_register_created_at`) VALUES
(1, 'sdfsafasdf', 'sdfsafasdf_CV_1538650852514.doc', 1, '2018-10-19 10:20:37'),
(2, 'test', 'test_CV_1539845221202.docx', 4, '2018-10-19 10:20:37');

-- --------------------------------------------------------

--
-- Table structure for table `domisili`
--

CREATE TABLE `domisili` (
  `id_domisili` int(11) NOT NULL,
  `domisili` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `domisili`
--

INSERT INTO `domisili` (`id_domisili`, `domisili`) VALUES
(1, 'Kota Bekasi'),
(2, 'Kabupaten Bekasi'),
(3, 'Jakarta Barat'),
(4, 'Jakarta Timur'),
(5, 'Jakarta Selatan'),
(6, 'Jakarta Utara'),
(7, 'Jakarta Pusat');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `nama_lengkap_guru` varchar(255) NOT NULL,
  `jenis_kelamin_id` int(11) NOT NULL,
  `agama_id` int(11) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guru_mengajar`
--

CREATE TABLE `guru_mengajar` (
  `id_guru_mengajar` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `mata_pelajaran_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `halaman_promosi`
--

CREATE TABLE `halaman_promosi` (
  `id_halaman_promosi` int(11) NOT NULL,
  `nama_halaman_promosi` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `isi_halaman_promosi` text NOT NULL,
  `image_halaman_promosi` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halaman_promosi`
--

INSERT INTO `halaman_promosi` (`id_halaman_promosi`, `nama_halaman_promosi`, `slug`, `isi_halaman_promosi`, `image_halaman_promosi`, `created_at`, `updated_at`) VALUES
(6, 'Bimbel', 'bimbel', '<div style=\"-en-clipboard: true;\">Telah dibuka <strong>Kelas Bimbel</strong>!</div>\r\n<div>&nbsp;</div>\r\n<div>Belajar dengan teman-teman jadi lebih seru dan pastinya tidak membuat kantong orang tua jebol!</div>\r\n<div>&nbsp;</div>\r\n<div>Tingkat kelas yang dibuka hanya untuk <strong>SMP</strong> dan <strong>SMA</strong>.</div>\r\n<div>&nbsp;</div>\r\n<div>Mata Pelajaran yang diajarkan:</div>\r\n<ol>\r\n<li>\r\n<div>Matematika</div>\r\n</li>\r\n<li>\r\n<div>Fisika</div>\r\n</li>\r\n<li>\r\n<div>Kimia</div>\r\n</li>\r\n<li>\r\n<div>Bahasa Inggris</div>\r\n</li>\r\n</ol>\r\n<div>&nbsp;</div>\r\n<div>Fasilitas MAX Education?</div>\r\n<ol>\r\n<li>Kami mempunyai tenaga pengajar yang ahli di mata pelajarannya</li>\r\n<li>Jumlah siswa maks : 8 orang / kelas</li>\r\n<li>Murid yang mengalami kebingungan materi akan dibantu secara personal</li>\r\n<li>Harga sangat bersaing</li>\r\n</ol>\r\n<div>&nbsp;</div>\r\n<div><strong>Harga Kelas Bimbel sedang promo</strong>:</div>\r\n<ul>\r\n<li>\r\n<div>&nbsp;SMP dari 400rb / bulan menjadi 250rb / bulan</div>\r\n</li>\r\n<li>\r\n<div>SMA dari 550rb / bulan menjadi 350rb / bulan</div>\r\n</li>\r\n</ul>\r\n<div>&nbsp;</div>\r\n<div>Ayo segera daftarkan Anak Anda sekarang dengan cara mengisi data di bawah ini!</div>\r\n<div>&nbsp;</div>\r\n<div>Waktu dan Kuota <strong>sangat terbatas</strong>.</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>', '1536572894701.jpg', '2018-09-10 09:48:14', '2018-09-10 09:51:24'),
(7, 'Kenapa Anak Banyak Alasan Kalau Disuruh Belajar?', 'kenapa-anak-banyak-alasan-kalau-disuruh-belajar', '<p class=\"MsoNormal\"><strong><em style=\"mso-bidi-font-style: normal;\">Kenapa ya anak saya punya banyak alasan kalau disuruh belajar?</em></strong></p>\r\n<p class=\"MsoNormal\"><strong><em style=\"mso-bidi-font-style: normal;\">Anak saya disuruh belajar, belum setengah jam, udah ga mau lagi belajar.</em></strong></p>\r\n<p class=\"MsoNormal\">Kalimat pertanyaan di atas mewakili ungkapan kegelisahan orang tua saat melihat anaknya &ldquo;malas&rdquo; belajar. Sering pula orang tua menegur anak jika sang anak malas atau ogah belajar.</p>\r\n<p class=\"MsoNormal\">Tapi tahukan mama? Bentuk teguran atau pola asuh yang tidak tepat untuk anak justru dapat menciptakan perilaku menyimpang lain dan semakin menurunkan motivasi belajar anak. Oleh karena itu perlu diketahui lebih lanjut, hal apa saja yang menjadi penyebab anak memiliki kecendrungan malas belajar.</p>\r\n<p class=\"MsoNormal\">Faktor yang menyebabkan anak malas belajar dapat berupa faktor inheren (dari dalam diri anak) maupun eksteren (dari luar diri anak, seperti lingkungan, keluarga, teman). Berikut ini merupakan penjelasan dari faktor yang menyebabkan malasnya anak belajar:</p>\r\n<p class=\"MsoNormal\"><!-- [if !supportLists]--><strong><span style=\"mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin;\"><span style=\"mso-list: Ignore;\">1.<span style=\"font-family: Times New Roman; font-size: xx-small;\">&nbsp;</span></span></span>Tidak merasa nyaman dengan lingkungannya</strong></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\">Anak cenderung akan malas untuk belajar jika menemukan lingkungan yang tidak sesuai dengan dirinya. Ruangan yang terlalu berisik yang mengganggu konsentrasinya, atau ruangan yang kurang cahaya yang membuat anak menjadi mengantuk. Atau kadang juga kesulitan beradaptasi dengan lingkungan yang baru (contohnya, peralihan anak yang berpindah sekolah)</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\"><!-- [if !supportLists]--><strong><span style=\"mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin;\"><span style=\"mso-list: Ignore;\">2.<span style=\"font-family: Times New Roman; font-size: xx-small;\">&nbsp;</span></span></span>Tidak cocok belajar dengan guru di sekolah</strong></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\">Menemukan guru yang tepat untuk anak tidak mudah. Ada beberapa anak yang cocok dengan guru di sekolahnya dan ada pula yang tidak. Hal tersebut dapat memberikan dampak negatif berkepanjangan, salah satunya demotivasi belajar bagi anak serta menurunnya prestasi anak.</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\"><!-- [if !supportLists]--><strong><span style=\"mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin;\"><span style=\"mso-list: Ignore;\">3.<span style=\"font-family: Times New Roman; font-size: xx-small;\">&nbsp;</span></span></span>Tidak menyukai pelajarannya</strong></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\">Setiap anak dibekali kemampuan kognitif dan kemampuan intelektual yang berbeda-beda. Tidak semua anak menyukai pelajaran yang didapatkannya di sekolah. Bagi anak yang tidak menyukai pelajarannya, cenderung akan mengeluhkan pelajarannya dan menghindari tantangan untuk dapat bisa memahami pelajaran tersebut. Sehingga anak menjadi bosan dan malas untuk belajar. Memaksakan anak untuk menyukai semua pelajaran akan menyulitkan anak dalam mencapai prestasi terbaiknya. Namun, mendukung anak pada pelajaran yang ia sukai bisa membuatnya semakin percaya diri dalam mencapai prestasi bahkan untuk pelajaran yang awalnya ia tidak suka</p>\r\n<p class=\"MsoListParagraphCxSpMiddle\"><!-- [if !supportLists]--><strong><span style=\"mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin;\"><span style=\"mso-list: Ignore;\">4.<span style=\"font-family: Times New Roman; font-size: xx-small;\">&nbsp;</span></span></span>Terlalu lelah</strong></p>\r\n<p class=\"MsoListParagraphCxSpLast\">Terlalu lelah bisa menjadi alasan mengapa seorang anak malas untuk belajar. Jarak dan waktu yang cukup panjang yang ditempuh seorang anak ke sekolah, beraktivitas seharian di sekolah, ditambah lagi kegiatan lain di luar sekolah, membuat seorang anak kelelahan. Saat seorang anak terlalu lelah, anak justru memilih untuk beristirahat, menghindari kejenuhannya dan malas untuk belajar, meskipun tugas sekolah harus dikerjakan ataupun ada ulangan di sekolah yang dihadapinya esok hari.</p>\r\n<p class=\"MsoListParagraphCxSpLast\">&nbsp;</p>\r\n<p class=\"MsoNormal\">Masih banyak faktor lainnya yang mempengaruhi mengapa seorang anak menjadi malas belajar. Untuk itu, perlu adanya solusi untuk mengatasi anak yang malas belajar, yaitu dukungan dari orang tua dalam menciptakan suasana belajar anak yang kondusif di rumah, mulai dari tata ruangan, pencahayaan serta menghindari kebisingan. Kemudian tidak memarahi anak karena membenci pelajaran atau guru di sekolahnya, tetapi mencarikan solusi yang tepat agar anak tetap bisa menguasai materi pelajaran di sekolahnya. Hal yang tepat diberikan untuk anak adalah pendampingan intensif dengan guru les privat yang bisa menjadi teman belajar anak. Les privat menjadi solusi yang tepat, orang tua dapat mengawasi langsung proses belajar anak dan guru, serta tidak lagi membuat anak terlalu lelah karena waktu belajar lebih fleksibel.</p>\r\n<p>&nbsp;</p>', '1540786463677.jpg', '2018-10-29 04:14:23', '2018-10-29 04:29:09');

-- --------------------------------------------------------

--
-- Table structure for table `hari`
--

CREATE TABLE `hari` (
  `id_hari` int(11) NOT NULL,
  `hari` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hari`
--

INSERT INTO `hari` (`id_hari`, `hari`) VALUES
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat'),
(6, 'Sabtu'),
(7, 'Minggu');

-- --------------------------------------------------------

--
-- Table structure for table `jam`
--

CREATE TABLE `jam` (
  `id_jam` int(11) NOT NULL,
  `jam` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam`
--

INSERT INTO `jam` (`id_jam`, `jam`) VALUES
(1, '08.00'),
(2, '09.00'),
(3, '10.00'),
(4, '11.00'),
(5, '12.00'),
(6, '13.00'),
(7, '14.00'),
(8, '15.00'),
(9, '16.00'),
(10, '17.00'),
(11, '18.00'),
(12, '19.00');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kelamin`
--

CREATE TABLE `jenis_kelamin` (
  `id_jenis_kelamin` int(11) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`id_jenis_kelamin`, `jenis_kelamin`) VALUES
(1, 'Laki - Laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `program_id` int(11) NOT NULL,
  `mata_pelajaran_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelas_comments`
--

CREATE TABLE `kelas_comments` (
  `id_kelas_comments` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `document` varchar(255) NOT NULL,
  `kelas_posts_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelas_posts`
--

CREATE TABLE `kelas_posts` (
  `id_kelas_posts` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `document` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id_mata_pelajaran` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `mata_pelajaran` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id_mata_pelajaran`, `program_id`, `mata_pelajaran`, `created_at`, `updated_at`) VALUES
(1, 1, 'Matematika', '2018-03-18 17:35:58', '2018-03-27 06:28:25'),
(2, 1, 'Bahasa Inggris', '2018-03-18 17:36:36', '2018-03-27 06:28:25'),
(3, 2, 'Matematika', '2018-03-18 17:39:26', '2018-03-27 06:28:25'),
(4, 2, 'IPA', '2018-03-18 17:37:32', '2018-03-27 06:28:25'),
(5, 2, 'Bahasa Inggris', '2018-03-18 17:37:41', '2018-03-27 06:28:25'),
(6, 3, 'Matematika', '2018-03-18 17:42:20', '2018-06-02 12:49:40'),
(7, 3, 'Fisika', '2018-03-18 17:42:29', '2018-03-27 06:28:25'),
(8, 3, 'Kimia', '2018-03-18 17:42:37', '2018-03-27 06:28:25'),
(9, 3, 'Ekonomi', '2018-03-18 17:42:43', '2018-03-27 06:28:25'),
(10, 3, 'Sosiologi', '2018-03-18 17:42:49', '2018-03-27 06:28:25'),
(11, 3, 'Geografi', '2018-03-18 17:42:53', '2018-06-10 06:13:59'),
(12, 3, 'Bahasa Inggris', '2018-03-18 17:43:03', '2018-03-27 06:28:25'),
(13, 4, 'Matematika', '2018-03-18 17:43:20', '2018-03-27 06:28:25'),
(14, 4, 'Fisika', '2018-03-18 17:43:24', '2018-03-27 06:28:25'),
(15, 4, 'Kimia', '2018-03-18 17:43:28', '2018-03-27 06:28:25'),
(17, 4, 'Ekonomi', '2018-03-18 17:43:40', '2018-03-27 06:28:25'),
(18, 4, 'Sosiologi', '2018-03-18 17:43:46', '2018-03-27 06:28:25'),
(19, 4, 'Geografi', '2018-03-18 17:43:52', '2018-03-27 06:28:25'),
(20, 5, 'Conversation', '2018-03-18 17:44:40', '2018-03-27 06:28:25'),
(21, 5, 'Reading', '2018-03-18 17:44:59', '2018-03-27 06:28:25'),
(22, 5, 'Writing', '2018-03-18 17:45:06', '2018-03-27 06:28:25'),
(23, 6, 'Guitar', '2018-03-18 17:45:31', '2018-03-27 06:28:25'),
(24, 6, 'Piano', '2018-03-18 17:45:36', '2018-03-27 06:28:25'),
(25, 6, 'Keyboard', '2018-03-18 17:45:44', '2018-03-27 06:28:25'),
(26, 6, 'Drumb', '2018-03-18 17:45:50', '2018-03-27 06:28:25'),
(27, 6, 'Violin', '2018-03-18 17:45:58', '2018-03-27 06:28:25'),
(28, 4, 'Bahasa Inggris', '2018-04-04 07:02:36', '2018-04-04 07:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id_paket` int(11) NOT NULL,
  `nama_paket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id_paket`, `nama_paket`) VALUES
(1, 'Private 1 Siswa'),
(2, 'Tag Team 2 Siswa'),
(3, 'Group 3 Siswa'),
(4, 'Group 4 Siswa'),
(5, 'School'),
(6, 'SBMPTN/TOEFL/TOIEC'),
(7, 'Conversation Pro');

-- --------------------------------------------------------

--
-- Table structure for table `paket_program`
--

CREATE TABLE `paket_program` (
  `id_paket_program` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `paket_id` int(11) NOT NULL,
  `harga_paket` int(11) NOT NULL,
  `waktu_belajar` int(11) NOT NULL,
  `jumlah_pertemuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket_program`
--

INSERT INTO `paket_program` (`id_paket_program`, `program_id`, `paket_id`, `harga_paket`, `waktu_belajar`, `jumlah_pertemuan`) VALUES
(8, 3, 1, 1200000, 2, 8),
(9, 3, 2, 1600000, 2, 8),
(10, 3, 3, 2000000, 2, 8),
(11, 3, 4, 2400000, 2, 8),
(12, 4, 1, 2000000, 2, 8),
(13, 4, 2, 3000000, 2, 8),
(14, 4, 3, 3750000, 2, 8),
(15, 4, 4, 4600000, 2, 8),
(16, 5, 5, 1200000, 2, 8),
(17, 5, 6, 2000000, 2, 8),
(18, 5, 7, 1600000, 2, 8),
(19, 6, 1, 800000, 2, 8),
(20, 2, 1, 800000, 2, 8),
(21, 2, 2, 1200000, 2, 8),
(22, 2, 3, 1600000, 2, 8),
(23, 2, 4, 2000000, 2, 8),
(24, 7, 1, 480000, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id_program` int(255) NOT NULL,
  `program` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id_program`, `program`, `created_at`, `updated_at`) VALUES
(1, 'SD', '2018-03-19 00:24:49', '2018-03-28 04:26:28'),
(2, 'SMP', '2018-03-19 00:24:49', '2018-03-24 10:17:20'),
(3, 'SMA', '2018-03-19 00:24:49', '2018-03-24 10:17:20'),
(4, 'SBMPTN', '2018-03-19 00:24:49', '2018-03-24 10:17:20'),
(5, 'English', '2018-03-19 00:24:49', '2018-03-24 10:17:20'),
(6, 'Music', '2018-03-19 00:24:49', '2018-03-24 10:17:20'),
(7, 'Mandarin', '2018-10-16 09:38:25', '2018-10-16 09:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `role`) VALUES
(1, 'Admin'),
(2, 'Guru'),
(3, 'Siswa');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nama_lengkap_siswa` varchar(255) NOT NULL,
  `jenis_kelamin_id` int(11) NOT NULL,
  `nomor_hp_siswa` varchar(25) NOT NULL,
  `nomor_hp_orang_tua` varchar(25) NOT NULL,
  `alamat_lengkap` text NOT NULL,
  `domisili_id` int(11) NOT NULL,
  `tingkat_sekolah_id` int(11) NOT NULL,
  `program_id` int(255) NOT NULL,
  `kelas` varchar(25) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nama_lengkap_siswa`, `jenis_kelamin_id`, `nomor_hp_siswa`, `nomor_hp_orang_tua`, `alamat_lengkap`, `domisili_id`, `tingkat_sekolah_id`, `program_id`, `kelas`, `avatar`, `registered`, `updated_at`) VALUES
(5, 'FARAND ', 1, '08161914602', '08161914602', 'JL. RAJAWALI 6 NO 54 BEKASI', 1, 2, 1, '9', '', '2018-09-21 09:21:06', '2018-09-21 09:21:06'),
(6, 'Jonathan', 1, '081808761185', '081808761185', 'Harapan baru regency blok a3 no 5 bekasi barat', 1, 2, 2, '7', '', '2018-10-17 02:30:26', '2018-10-17 02:30:26'),
(7, 'Nathanael', 1, '081808761185', '081808761185', 'Harapan baru regency blok a3 no 5 bekasi barat', 1, 2, 2, '7', '', '2018-10-17 02:32:24', '2018-10-17 02:32:24'),
(8, 'test', 1, '123', '123', '123', 1, 3, 3, '123', '', '2018-10-17 04:47:31', '2018-10-17 04:47:31'),
(9, 'test', 1, '121', '123', '123', 1, 3, 4, '123', '', '2018-10-17 04:48:45', '2018-10-17 04:48:45'),
(10, 'tset', 1, '123', '132', '123', 1, 4, 5, '12', '', '2018-10-17 04:50:29', '2018-10-17 04:50:29'),
(11, 'test', 1, '123', '123', '132', 1, 4, 6, '12', '', '2018-10-17 04:52:05', '2018-10-17 04:52:05'),
(12, 'Kate Middleson', 2, '081325738941', '087748319238', 'Jalan Percobaan', 7, 3, 2, '2', '', '2018-10-27 05:53:42', '2018-10-27 05:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_absensi`
--

CREATE TABLE `siswa_absensi` (
  `id_siswa_absensi` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `mata_pelajaran_id` int(11) NOT NULL,
  `hari_id` int(11) NOT NULL,
  `jam_id` int(11) NOT NULL,
  `laporan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa_belajar_gratis`
--

CREATE TABLE `siswa_belajar_gratis` (
  `id_siswa_belajar_gratis` int(11) NOT NULL,
  `nama_lengkap_siswa` varchar(255) NOT NULL,
  `gelombang` varchar(255) NOT NULL,
  `nama_ig` varchar(255) NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa_detail_hari`
--

CREATE TABLE `siswa_detail_hari` (
  `id_siswa_detail_hari` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `hari_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_detail_hari`
--

INSERT INTO `siswa_detail_hari` (`id_siswa_detail_hari`, `siswa_id`, `hari_id`, `created_at`, `updated_at`) VALUES
(7, 5, 3, '2018-09-21 09:21:06', '2018-09-21 09:21:06'),
(8, 6, 1, '2018-10-17 02:30:26', '2018-10-17 02:30:26'),
(9, 6, 3, '2018-10-17 02:30:26', '2018-10-17 02:30:26'),
(10, 7, 2, '2018-10-17 02:32:24', '2018-10-17 02:32:24'),
(11, 7, 4, '2018-10-17 02:32:24', '2018-10-17 02:32:24'),
(12, 8, 1, '2018-10-17 04:47:31', '2018-10-17 04:47:31'),
(13, 9, 2, '2018-10-17 04:48:45', '2018-10-17 04:48:45'),
(14, 10, 2, '2018-10-17 04:50:29', '2018-10-17 04:50:29'),
(15, 11, 2, '2018-10-17 04:52:05', '2018-10-17 04:52:05'),
(16, 12, 6, '2018-10-27 05:53:42', '2018-10-27 05:53:42'),
(17, 12, 7, '2018-10-27 05:53:42', '2018-10-27 05:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_detail_jam`
--

CREATE TABLE `siswa_detail_jam` (
  `id_siswa_detail_jam` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `jam_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_detail_jam`
--

INSERT INTO `siswa_detail_jam` (`id_siswa_detail_jam`, `siswa_id`, `jam_id`, `created_at`, `updated_at`) VALUES
(6, 5, 10, '2018-09-21 09:21:06', '2018-09-21 09:21:06'),
(7, 6, 11, '2018-10-17 02:30:26', '2018-10-17 02:30:26'),
(8, 7, 11, '2018-10-17 02:32:24', '2018-10-17 02:32:24'),
(9, 7, 12, '2018-10-17 02:32:24', '2018-10-17 02:32:24'),
(10, 8, 1, '2018-10-17 04:47:31', '2018-10-17 04:47:31'),
(11, 9, 5, '2018-10-17 04:48:45', '2018-10-17 04:48:45'),
(12, 10, 2, '2018-10-17 04:50:29', '2018-10-17 04:50:29'),
(13, 11, 2, '2018-10-17 04:52:05', '2018-10-17 04:52:05'),
(14, 12, 1, '2018-10-27 05:53:42', '2018-10-27 05:53:42'),
(15, 12, 7, '2018-10-27 05:53:42', '2018-10-27 05:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_detail_mata_pelajaran`
--

CREATE TABLE `siswa_detail_mata_pelajaran` (
  `id_siswa_detail_mata_pelajaran` int(11) NOT NULL,
  `siswa_id` int(255) NOT NULL,
  `mata_pelajaran_id` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_detail_mata_pelajaran`
--

INSERT INTO `siswa_detail_mata_pelajaran` (`id_siswa_detail_mata_pelajaran`, `siswa_id`, `mata_pelajaran_id`, `created_at`, `updated_at`) VALUES
(7, 5, 1, '2018-09-21 09:21:06', '2018-09-21 09:21:06'),
(8, 6, 3, '2018-10-17 02:30:26', '2018-10-17 02:30:26'),
(9, 7, 3, '2018-10-17 02:32:24', '2018-10-17 02:32:24'),
(10, 8, 6, '2018-10-17 04:47:31', '2018-10-17 04:47:31'),
(11, 9, 13, '2018-10-17 04:48:45', '2018-10-17 04:48:45'),
(12, 10, 20, '2018-10-17 04:50:29', '2018-10-17 04:50:29'),
(13, 11, 24, '2018-10-17 04:52:05', '2018-10-17 04:52:05'),
(14, 12, 3, '2018-10-27 05:53:42', '2018-10-27 05:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_jadwal`
--

CREATE TABLE `siswa_jadwal` (
  `id_siswa_jadwal` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `hari_id` int(11) NOT NULL,
  `jam_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `mata_pelajaran_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa_promosi`
--

CREATE TABLE `siswa_promosi` (
  `id_siswa_promosi` int(11) NOT NULL,
  `nama_lengkap_siswa_promosi` varchar(255) NOT NULL,
  `jenis_kelamin_id` int(11) NOT NULL,
  `email_siswa_promosi` varchar(255) NOT NULL,
  `alamat_lengkap_siswa_promosi` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nomor_hp_siswa_promosi` varchar(255) NOT NULL,
  `halaman_promosi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_promosi`
--

INSERT INTO `siswa_promosi` (`id_siswa_promosi`, `nama_lengkap_siswa_promosi`, `jenis_kelamin_id`, `email_siswa_promosi`, `alamat_lengkap_siswa_promosi`, `created_at`, `nomor_hp_siswa_promosi`, `halaman_promosi_id`) VALUES
(1, 'Ambar Tri Utami', 2, 'ambartriutami20@gmail.com', 'Taman sari Maphar Sawah besar Jakarta barat', '2018-09-09 13:08:07', '082220542062', 0),
(2, 'Ambar Tri Utami', 2, 'ambartriutami20@gmail.com', 'Tamansari Maphar Tamansari Jakarta barat', '2018-09-09 13:09:58', '082220542062', 0),
(3, 'Wijaya', 1, 'maxeducationindonesia@gmail.com', '123123', '2018-10-17 04:54:29', '21312', 6),
(4, 'Refaldo Bonatua Christian Siburian', 1, 'renandussiburian@yahoo.com', 'Bukit Golf Riverside. Cluster Riverside II blok B.01/60.(Jln.Leuwinanggung).Bojong Nangka.Gn.Putri.Rt/Rw:002/022.Bogor-Jawa Barat.16963', '2018-11-01 00:43:52', '081213558629', 7),
(5, 'Refaldo Bonatua Christian Siburian', 1, 'renandussiburian@yahoo.com', 'Bukit Golf Riverside. Cluster Riverside II blok B.01/60.(Jln.Leuwinanggung).Bojong Nangka.Gn.Putri.Rt/Rw:002/022.Bogor-Jawa Barat.16963', '2018-11-01 00:44:07', '081213558629', 7);

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE `slideshow` (
  `id_slideshow` int(11) NOT NULL,
  `image_slideshow` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id_slideshow`, `image_slideshow`, `created_at`) VALUES
(1, '1522825095797.jpg', '2018-04-04 06:58:15'),
(2, '1522825142468.jpeg', '2018-04-04 06:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat_sekolah`
--

CREATE TABLE `tingkat_sekolah` (
  `id_tingkat_sekolah` int(11) NOT NULL,
  `tingkat_sekolah` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tingkat_sekolah`
--

INSERT INTO `tingkat_sekolah` (`id_tingkat_sekolah`, `tingkat_sekolah`) VALUES
(1, 'SD'),
(2, 'SMP'),
(3, 'SMA IPA'),
(4, 'SMA IPS'),
(5, 'SMK');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `role_id`, `admin_id`, `guru_id`, `siswa_id`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '$2y$10$5Wsu6bxtoQEdPGE8BGr7l.XL/x8SVr9iwqELbvzP8IJUtuXuloPvO', 1, 1, 0, 0, '2018-08-14 00:46:24', '2018-08-14 00:46:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `agama`
--
ALTER TABLE `agama`
  ADD PRIMARY KEY (`id_agama`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id_career`);

--
-- Indexes for table `career_job_area`
--
ALTER TABLE `career_job_area`
  ADD PRIMARY KEY (`id_career_job_area`);

--
-- Indexes for table `career_job_position`
--
ALTER TABLE `career_job_position`
  ADD PRIMARY KEY (`id_career_job_position`);

--
-- Indexes for table `career_register`
--
ALTER TABLE `career_register`
  ADD PRIMARY KEY (`id_career_register`);

--
-- Indexes for table `domisili`
--
ALTER TABLE `domisili`
  ADD PRIMARY KEY (`id_domisili`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `guru_mengajar`
--
ALTER TABLE `guru_mengajar`
  ADD PRIMARY KEY (`id_guru_mengajar`);

--
-- Indexes for table `halaman_promosi`
--
ALTER TABLE `halaman_promosi`
  ADD PRIMARY KEY (`id_halaman_promosi`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id_hari`);

--
-- Indexes for table `jam`
--
ALTER TABLE `jam`
  ADD PRIMARY KEY (`id_jam`);

--
-- Indexes for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  ADD PRIMARY KEY (`id_jenis_kelamin`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `kelas_comments`
--
ALTER TABLE `kelas_comments`
  ADD PRIMARY KEY (`id_kelas_comments`);

--
-- Indexes for table `kelas_posts`
--
ALTER TABLE `kelas_posts`
  ADD PRIMARY KEY (`id_kelas_posts`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id_mata_pelajaran`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `paket_program`
--
ALTER TABLE `paket_program`
  ADD PRIMARY KEY (`id_paket_program`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id_program`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `siswa_absensi`
--
ALTER TABLE `siswa_absensi`
  ADD PRIMARY KEY (`id_siswa_absensi`);

--
-- Indexes for table `siswa_belajar_gratis`
--
ALTER TABLE `siswa_belajar_gratis`
  ADD PRIMARY KEY (`id_siswa_belajar_gratis`);

--
-- Indexes for table `siswa_detail_hari`
--
ALTER TABLE `siswa_detail_hari`
  ADD PRIMARY KEY (`id_siswa_detail_hari`);

--
-- Indexes for table `siswa_detail_jam`
--
ALTER TABLE `siswa_detail_jam`
  ADD PRIMARY KEY (`id_siswa_detail_jam`);

--
-- Indexes for table `siswa_detail_mata_pelajaran`
--
ALTER TABLE `siswa_detail_mata_pelajaran`
  ADD PRIMARY KEY (`id_siswa_detail_mata_pelajaran`);

--
-- Indexes for table `siswa_jadwal`
--
ALTER TABLE `siswa_jadwal`
  ADD PRIMARY KEY (`id_siswa_jadwal`);

--
-- Indexes for table `siswa_promosi`
--
ALTER TABLE `siswa_promosi`
  ADD PRIMARY KEY (`id_siswa_promosi`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id_slideshow`);

--
-- Indexes for table `tingkat_sekolah`
--
ALTER TABLE `tingkat_sekolah`
  ADD PRIMARY KEY (`id_tingkat_sekolah`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `agama`
--
ALTER TABLE `agama`
  MODIFY `id_agama` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id_career` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `career_job_area`
--
ALTER TABLE `career_job_area`
  MODIFY `id_career_job_area` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `career_job_position`
--
ALTER TABLE `career_job_position`
  MODIFY `id_career_job_position` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `career_register`
--
ALTER TABLE `career_register`
  MODIFY `id_career_register` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `domisili`
--
ALTER TABLE `domisili`
  MODIFY `id_domisili` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guru_mengajar`
--
ALTER TABLE `guru_mengajar`
  MODIFY `id_guru_mengajar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `halaman_promosi`
--
ALTER TABLE `halaman_promosi`
  MODIFY `id_halaman_promosi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id_hari` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jam`
--
ALTER TABLE `jam`
  MODIFY `id_jam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  MODIFY `id_jenis_kelamin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelas_comments`
--
ALTER TABLE `kelas_comments`
  MODIFY `id_kelas_comments` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelas_posts`
--
ALTER TABLE `kelas_posts`
  MODIFY `id_kelas_posts` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id_mata_pelajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id_paket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `paket_program`
--
ALTER TABLE `paket_program`
  MODIFY `id_paket_program` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id_program` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `siswa_absensi`
--
ALTER TABLE `siswa_absensi`
  MODIFY `id_siswa_absensi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa_belajar_gratis`
--
ALTER TABLE `siswa_belajar_gratis`
  MODIFY `id_siswa_belajar_gratis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa_detail_hari`
--
ALTER TABLE `siswa_detail_hari`
  MODIFY `id_siswa_detail_hari` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `siswa_detail_jam`
--
ALTER TABLE `siswa_detail_jam`
  MODIFY `id_siswa_detail_jam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `siswa_detail_mata_pelajaran`
--
ALTER TABLE `siswa_detail_mata_pelajaran`
  MODIFY `id_siswa_detail_mata_pelajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `siswa_jadwal`
--
ALTER TABLE `siswa_jadwal`
  MODIFY `id_siswa_jadwal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswa_promosi`
--
ALTER TABLE `siswa_promosi`
  MODIFY `id_siswa_promosi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id_slideshow` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tingkat_sekolah`
--
ALTER TABLE `tingkat_sekolah`
  MODIFY `id_tingkat_sekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
